﻿namespace EldaVideo
{
    partial class Prodinshop
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_tnsforder = new System.Windows.Forms.DataGridView();
            this.tb_price = new System.Windows.Forms.TextBox();
            this.tb_war = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_prodinshop_apply = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tnsforder)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_tnsforder
            // 
            this.dgv_tnsforder.AllowUserToAddRows = false;
            this.dgv_tnsforder.AllowUserToDeleteRows = false;
            this.dgv_tnsforder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_tnsforder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_tnsforder.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_tnsforder.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_tnsforder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_tnsforder.Location = new System.Drawing.Point(1, 1);
            this.dgv_tnsforder.Name = "dgv_tnsforder";
            this.dgv_tnsforder.Size = new System.Drawing.Size(973, 323);
            this.dgv_tnsforder.TabIndex = 3;
            this.dgv_tnsforder.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_tnsforder_CellClick);
            // 
            // tb_price
            // 
            this.tb_price.Location = new System.Drawing.Point(490, 336);
            this.tb_price.Name = "tb_price";
            this.tb_price.Size = new System.Drawing.Size(219, 20);
            this.tb_price.TabIndex = 4;
            // 
            // tb_war
            // 
            this.tb_war.Location = new System.Drawing.Point(490, 372);
            this.tb_war.Name = "tb_war";
            this.tb_war.Size = new System.Drawing.Size(219, 20);
            this.tb_war.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(297, 337);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "Укажите цену:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(297, 370);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 19);
            this.label2.TabIndex = 7;
            this.label2.Text = "Укажите гарантию:";
            // 
            // btn_prodinshop_apply
            // 
            this.btn_prodinshop_apply.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodinshop_apply.Location = new System.Drawing.Point(847, 364);
            this.btn_prodinshop_apply.Name = "btn_prodinshop_apply";
            this.btn_prodinshop_apply.Size = new System.Drawing.Size(117, 31);
            this.btn_prodinshop_apply.TabIndex = 8;
            this.btn_prodinshop_apply.Text = "Применить";
            this.btn_prodinshop_apply.UseVisualStyleBackColor = true;
            this.btn_prodinshop_apply.Click += new System.EventHandler(this.btn_prodinshop_apply_Click);
            // 
            // Prodinshop
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(976, 398);
            this.Controls.Add(this.btn_prodinshop_apply);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_war);
            this.Controls.Add(this.tb_price);
            this.Controls.Add(this.dgv_tnsforder);
            this.Name = "Prodinshop";
            this.Text = "Добавление товара в магазин из заказов на перевозку";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tnsforder)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_tnsforder;
        private System.Windows.Forms.TextBox tb_price;
        private System.Windows.Forms.TextBox tb_war;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_prodinshop_apply;
    }
}