﻿namespace EldaVideo
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab_control = new System.Windows.Forms.TabControl();
            this.tab_prodinstock = new System.Windows.Forms.TabPage();
            this.btn_prodinstock_del = new System.Windows.Forms.Button();
            this.btn_prodinstock_add = new System.Windows.Forms.Button();
            this.dgv_prodinstock = new System.Windows.Forms.DataGridView();
            this.tab_admin = new System.Windows.Forms.TabPage();
            this.btn_query = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rtb_logs = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rtb_query = new System.Windows.Forms.RichTextBox();
            this.tab_prodinshop = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_war = new System.Windows.Forms.TextBox();
            this.tb_price = new System.Windows.Forms.TextBox();
            this.btn_prodinshop_upd = new System.Windows.Forms.Button();
            this.btn_prodinshop_del = new System.Windows.Forms.Button();
            this.btn_prodinshop_add = new System.Windows.Forms.Button();
            this.dgv_prodinshop = new System.Windows.Forms.DataGridView();
            this.tab_prodlist = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_prodlist_man = new System.Windows.Forms.TextBox();
            this.tb_prodlist_name = new System.Windows.Forms.TextBox();
            this.btn_prodlist_upd = new System.Windows.Forms.Button();
            this.btn_prodlist_del = new System.Windows.Forms.Button();
            this.btn_prodlist_add = new System.Windows.Forms.Button();
            this.dgv_prodlist = new System.Windows.Forms.DataGridView();
            this.tab_prodtype = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_typename = new System.Windows.Forms.TextBox();
            this.btn_prodtype_upd = new System.Windows.Forms.Button();
            this.btn_prodtype_del = new System.Windows.Forms.Button();
            this.btn_prodtype_add = new System.Windows.Forms.Button();
            this.dgv_prodtype = new System.Windows.Forms.DataGridView();
            this.tab_delivery = new System.Windows.Forms.TabPage();
            this.dgv_delivery = new System.Windows.Forms.DataGridView();
            this.tab_orders = new System.Windows.Forms.TabPage();
            this.btn_servicectr_add = new System.Windows.Forms.Button();
            this.btn_addlog = new System.Windows.Forms.Button();
            this.btn_orders_del = new System.Windows.Forms.Button();
            this.btn_orders_upd = new System.Windows.Forms.Button();
            this.btn_orders_add = new System.Windows.Forms.Button();
            this.dgv_orders = new System.Windows.Forms.DataGridView();
            this.tab_serviceord = new System.Windows.Forms.TabPage();
            this.btn_stopped = new System.Windows.Forms.Button();
            this.btn_done = new System.Windows.Forms.Button();
            this.btn_decline = new System.Windows.Forms.Button();
            this.btn_inprogress = new System.Windows.Forms.Button();
            this.dgv_serviceord = new System.Windows.Forms.DataGridView();
            this.tab_tnsforder = new System.Windows.Forms.TabPage();
            this.dgv_tnsforder = new System.Windows.Forms.DataGridView();
            this.tab_shops = new System.Windows.Forms.TabPage();
            this.dgv_shops = new System.Windows.Forms.DataGridView();
            this.tab_servicectr = new System.Windows.Forms.TabPage();
            this.dgv_servicectr = new System.Windows.Forms.DataGridView();
            this.tab_servicelog = new System.Windows.Forms.TabPage();
            this.dgv_logservice = new System.Windows.Forms.DataGridView();
            this.tab_offices = new System.Windows.Forms.TabPage();
            this.dgv_offices = new System.Windows.Forms.DataGridView();
            this.tab_employees = new System.Windows.Forms.TabPage();
            this.btn_employees_add = new System.Windows.Forms.Button();
            this.dgv_employees = new System.Windows.Forms.DataGridView();
            this.tab_positions = new System.Windows.Forms.TabPage();
            this.dgv_positions = new System.Windows.Forms.DataGridView();
            this.tab_suppliers = new System.Windows.Forms.TabPage();
            this.dgv_suppliers = new System.Windows.Forms.DataGridView();
            this.tab_reports = new System.Windows.Forms.TabPage();
            this.link_employees = new System.Windows.Forms.LinkLabel();
            this.link_profit = new System.Windows.Forms.LinkLabel();
            this.btn_rep = new System.Windows.Forms.Button();
            this.tab_control.SuspendLayout();
            this.tab_prodinstock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodinstock)).BeginInit();
            this.tab_admin.SuspendLayout();
            this.tab_prodinshop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodinshop)).BeginInit();
            this.tab_prodlist.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodlist)).BeginInit();
            this.tab_prodtype.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodtype)).BeginInit();
            this.tab_delivery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_delivery)).BeginInit();
            this.tab_orders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_orders)).BeginInit();
            this.tab_serviceord.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_serviceord)).BeginInit();
            this.tab_tnsforder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tnsforder)).BeginInit();
            this.tab_shops.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_shops)).BeginInit();
            this.tab_servicectr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_servicectr)).BeginInit();
            this.tab_servicelog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_logservice)).BeginInit();
            this.tab_offices.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_offices)).BeginInit();
            this.tab_employees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_employees)).BeginInit();
            this.tab_positions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_positions)).BeginInit();
            this.tab_suppliers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_suppliers)).BeginInit();
            this.tab_reports.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab_control
            // 
            this.tab_control.Controls.Add(this.tab_prodinstock);
            this.tab_control.Controls.Add(this.tab_admin);
            this.tab_control.Controls.Add(this.tab_prodinshop);
            this.tab_control.Controls.Add(this.tab_prodlist);
            this.tab_control.Controls.Add(this.tab_prodtype);
            this.tab_control.Controls.Add(this.tab_delivery);
            this.tab_control.Controls.Add(this.tab_orders);
            this.tab_control.Controls.Add(this.tab_serviceord);
            this.tab_control.Controls.Add(this.tab_tnsforder);
            this.tab_control.Controls.Add(this.tab_shops);
            this.tab_control.Controls.Add(this.tab_servicectr);
            this.tab_control.Controls.Add(this.tab_servicelog);
            this.tab_control.Controls.Add(this.tab_offices);
            this.tab_control.Controls.Add(this.tab_employees);
            this.tab_control.Controls.Add(this.tab_positions);
            this.tab_control.Controls.Add(this.tab_suppliers);
            this.tab_control.Controls.Add(this.tab_reports);
            this.tab_control.Location = new System.Drawing.Point(-3, 1);
            this.tab_control.Name = "tab_control";
            this.tab_control.SelectedIndex = 0;
            this.tab_control.Size = new System.Drawing.Size(979, 469);
            this.tab_control.TabIndex = 0;
            // 
            // tab_prodinstock
            // 
            this.tab_prodinstock.BackColor = System.Drawing.Color.LightGray;
            this.tab_prodinstock.Controls.Add(this.btn_rep);
            this.tab_prodinstock.Controls.Add(this.btn_prodinstock_del);
            this.tab_prodinstock.Controls.Add(this.btn_prodinstock_add);
            this.tab_prodinstock.Controls.Add(this.dgv_prodinstock);
            this.tab_prodinstock.Location = new System.Drawing.Point(4, 22);
            this.tab_prodinstock.Name = "tab_prodinstock";
            this.tab_prodinstock.Padding = new System.Windows.Forms.Padding(3);
            this.tab_prodinstock.Size = new System.Drawing.Size(971, 443);
            this.tab_prodinstock.TabIndex = 0;
            this.tab_prodinstock.Text = "Товары на складе";
            this.tab_prodinstock.Click += new System.EventHandler(this.tab_prodinstock_Click);
            // 
            // btn_prodinstock_del
            // 
            this.btn_prodinstock_del.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodinstock_del.Location = new System.Drawing.Point(830, 337);
            this.btn_prodinstock_del.Name = "btn_prodinstock_del";
            this.btn_prodinstock_del.Size = new System.Drawing.Size(117, 31);
            this.btn_prodinstock_del.TabIndex = 3;
            this.btn_prodinstock_del.Text = "Удалить";
            this.btn_prodinstock_del.UseVisualStyleBackColor = true;
            this.btn_prodinstock_del.Click += new System.EventHandler(this.btn_prodinstock_del_Click);
            // 
            // btn_prodinstock_add
            // 
            this.btn_prodinstock_add.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodinstock_add.Location = new System.Drawing.Point(696, 337);
            this.btn_prodinstock_add.Name = "btn_prodinstock_add";
            this.btn_prodinstock_add.Size = new System.Drawing.Size(117, 31);
            this.btn_prodinstock_add.TabIndex = 1;
            this.btn_prodinstock_add.Text = "Добавить";
            this.btn_prodinstock_add.UseVisualStyleBackColor = true;
            this.btn_prodinstock_add.Click += new System.EventHandler(this.btn_prodinstock_add_Click);
            // 
            // dgv_prodinstock
            // 
            this.dgv_prodinstock.AllowUserToAddRows = false;
            this.dgv_prodinstock.AllowUserToDeleteRows = false;
            this.dgv_prodinstock.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_prodinstock.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_prodinstock.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_prodinstock.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_prodinstock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_prodinstock.Location = new System.Drawing.Point(6, 6);
            this.dgv_prodinstock.Name = "dgv_prodinstock";
            this.dgv_prodinstock.ReadOnly = true;
            this.dgv_prodinstock.Size = new System.Drawing.Size(962, 323);
            this.dgv_prodinstock.TabIndex = 0;
            // 
            // tab_admin
            // 
            this.tab_admin.BackColor = System.Drawing.Color.LightGray;
            this.tab_admin.Controls.Add(this.btn_query);
            this.tab_admin.Controls.Add(this.label2);
            this.tab_admin.Controls.Add(this.rtb_logs);
            this.tab_admin.Controls.Add(this.label1);
            this.tab_admin.Controls.Add(this.rtb_query);
            this.tab_admin.Location = new System.Drawing.Point(4, 22);
            this.tab_admin.Name = "tab_admin";
            this.tab_admin.Padding = new System.Windows.Forms.Padding(3);
            this.tab_admin.Size = new System.Drawing.Size(971, 443);
            this.tab_admin.TabIndex = 1;
            this.tab_admin.Text = "Консоль";
            // 
            // btn_query
            // 
            this.btn_query.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_query.Location = new System.Drawing.Point(445, 201);
            this.btn_query.Name = "btn_query";
            this.btn_query.Size = new System.Drawing.Size(86, 28);
            this.btn_query.TabIndex = 4;
            this.btn_query.Text = "query";
            this.btn_query.UseVisualStyleBackColor = true;
            this.btn_query.Click += new System.EventHandler(this.btn_query_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(11, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Logs:";
            // 
            // rtb_logs
            // 
            this.rtb_logs.Location = new System.Drawing.Point(11, 256);
            this.rtb_logs.Name = "rtb_logs";
            this.rtb_logs.Size = new System.Drawing.Size(952, 181);
            this.rtb_logs.TabIndex = 2;
            this.rtb_logs.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(11, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "SQL QUERY";
            // 
            // rtb_query
            // 
            this.rtb_query.Location = new System.Drawing.Point(11, 28);
            this.rtb_query.Name = "rtb_query";
            this.rtb_query.Size = new System.Drawing.Size(952, 167);
            this.rtb_query.TabIndex = 0;
            this.rtb_query.Text = "";
            // 
            // tab_prodinshop
            // 
            this.tab_prodinshop.BackColor = System.Drawing.Color.LightGray;
            this.tab_prodinshop.Controls.Add(this.label3);
            this.tab_prodinshop.Controls.Add(this.label4);
            this.tab_prodinshop.Controls.Add(this.tb_war);
            this.tab_prodinshop.Controls.Add(this.tb_price);
            this.tab_prodinshop.Controls.Add(this.btn_prodinshop_upd);
            this.tab_prodinshop.Controls.Add(this.btn_prodinshop_del);
            this.tab_prodinshop.Controls.Add(this.btn_prodinshop_add);
            this.tab_prodinshop.Controls.Add(this.dgv_prodinshop);
            this.tab_prodinshop.Location = new System.Drawing.Point(4, 22);
            this.tab_prodinshop.Name = "tab_prodinshop";
            this.tab_prodinshop.Padding = new System.Windows.Forms.Padding(3);
            this.tab_prodinshop.Size = new System.Drawing.Size(971, 443);
            this.tab_prodinshop.TabIndex = 2;
            this.tab_prodinshop.Text = "Товары в наличие";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(229, 347);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 19);
            this.label3.TabIndex = 9;
            this.label3.Text = "Гарантия:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(9, 347);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 19);
            this.label4.TabIndex = 8;
            this.label4.Text = "Цена:";
            // 
            // tb_war
            // 
            this.tb_war.Location = new System.Drawing.Point(335, 347);
            this.tb_war.Name = "tb_war";
            this.tb_war.Size = new System.Drawing.Size(130, 20);
            this.tb_war.TabIndex = 8;
            // 
            // tb_price
            // 
            this.tb_price.Location = new System.Drawing.Point(67, 346);
            this.tb_price.Name = "tb_price";
            this.tb_price.Size = new System.Drawing.Size(136, 20);
            this.tb_price.TabIndex = 7;
            // 
            // btn_prodinshop_upd
            // 
            this.btn_prodinshop_upd.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodinshop_upd.Location = new System.Drawing.Point(723, 341);
            this.btn_prodinshop_upd.Name = "btn_prodinshop_upd";
            this.btn_prodinshop_upd.Size = new System.Drawing.Size(117, 31);
            this.btn_prodinshop_upd.TabIndex = 6;
            this.btn_prodinshop_upd.Text = "Обновить";
            this.btn_prodinshop_upd.UseVisualStyleBackColor = true;
            this.btn_prodinshop_upd.Click += new System.EventHandler(this.btn_prodinshop_upd_Click);
            // 
            // btn_prodinshop_del
            // 
            this.btn_prodinshop_del.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodinshop_del.Location = new System.Drawing.Point(846, 341);
            this.btn_prodinshop_del.Name = "btn_prodinshop_del";
            this.btn_prodinshop_del.Size = new System.Drawing.Size(117, 31);
            this.btn_prodinshop_del.TabIndex = 5;
            this.btn_prodinshop_del.Text = "Удалить";
            this.btn_prodinshop_del.UseVisualStyleBackColor = true;
            this.btn_prodinshop_del.Click += new System.EventHandler(this.btn_prodinshop_del_Click);
            // 
            // btn_prodinshop_add
            // 
            this.btn_prodinshop_add.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodinshop_add.Location = new System.Drawing.Point(598, 341);
            this.btn_prodinshop_add.Name = "btn_prodinshop_add";
            this.btn_prodinshop_add.Size = new System.Drawing.Size(117, 31);
            this.btn_prodinshop_add.TabIndex = 4;
            this.btn_prodinshop_add.Text = "Добавить";
            this.btn_prodinshop_add.UseVisualStyleBackColor = true;
            this.btn_prodinshop_add.Click += new System.EventHandler(this.btn_prodinshop_add_Click);
            // 
            // dgv_prodinshop
            // 
            this.dgv_prodinshop.AllowUserToAddRows = false;
            this.dgv_prodinshop.AllowUserToDeleteRows = false;
            this.dgv_prodinshop.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_prodinshop.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_prodinshop.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_prodinshop.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_prodinshop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_prodinshop.Location = new System.Drawing.Point(3, 6);
            this.dgv_prodinshop.Name = "dgv_prodinshop";
            this.dgv_prodinshop.ReadOnly = true;
            this.dgv_prodinshop.Size = new System.Drawing.Size(962, 323);
            this.dgv_prodinshop.TabIndex = 1;
            this.dgv_prodinshop.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_prodinshop_CellClick);
            // 
            // tab_prodlist
            // 
            this.tab_prodlist.BackColor = System.Drawing.Color.LightGray;
            this.tab_prodlist.Controls.Add(this.label6);
            this.tab_prodlist.Controls.Add(this.label5);
            this.tab_prodlist.Controls.Add(this.tb_prodlist_man);
            this.tab_prodlist.Controls.Add(this.tb_prodlist_name);
            this.tab_prodlist.Controls.Add(this.btn_prodlist_upd);
            this.tab_prodlist.Controls.Add(this.btn_prodlist_del);
            this.tab_prodlist.Controls.Add(this.btn_prodlist_add);
            this.tab_prodlist.Controls.Add(this.dgv_prodlist);
            this.tab_prodlist.Location = new System.Drawing.Point(4, 22);
            this.tab_prodlist.Name = "tab_prodlist";
            this.tab_prodlist.Padding = new System.Windows.Forms.Padding(3);
            this.tab_prodlist.Size = new System.Drawing.Size(971, 443);
            this.tab_prodlist.TabIndex = 3;
            this.tab_prodlist.Text = "Список товаров";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(11, 358);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 19);
            this.label6.TabIndex = 13;
            this.label6.Text = "Производитель";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(11, 333);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 19);
            this.label5.TabIndex = 12;
            this.label5.Text = "Название";
            // 
            // tb_prodlist_man
            // 
            this.tb_prodlist_man.Location = new System.Drawing.Point(146, 358);
            this.tb_prodlist_man.Name = "tb_prodlist_man";
            this.tb_prodlist_man.Size = new System.Drawing.Size(269, 20);
            this.tb_prodlist_man.TabIndex = 11;
            // 
            // tb_prodlist_name
            // 
            this.tb_prodlist_name.Location = new System.Drawing.Point(146, 332);
            this.tb_prodlist_name.Name = "tb_prodlist_name";
            this.tb_prodlist_name.Size = new System.Drawing.Size(269, 20);
            this.tb_prodlist_name.TabIndex = 10;
            // 
            // btn_prodlist_upd
            // 
            this.btn_prodlist_upd.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodlist_upd.Location = new System.Drawing.Point(723, 346);
            this.btn_prodlist_upd.Name = "btn_prodlist_upd";
            this.btn_prodlist_upd.Size = new System.Drawing.Size(117, 31);
            this.btn_prodlist_upd.TabIndex = 9;
            this.btn_prodlist_upd.Text = "Обновить";
            this.btn_prodlist_upd.UseVisualStyleBackColor = true;
            this.btn_prodlist_upd.Click += new System.EventHandler(this.btn_prodlist_upd_Click);
            // 
            // btn_prodlist_del
            // 
            this.btn_prodlist_del.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodlist_del.Location = new System.Drawing.Point(846, 346);
            this.btn_prodlist_del.Name = "btn_prodlist_del";
            this.btn_prodlist_del.Size = new System.Drawing.Size(117, 31);
            this.btn_prodlist_del.TabIndex = 8;
            this.btn_prodlist_del.Text = "Удалить";
            this.btn_prodlist_del.UseVisualStyleBackColor = true;
            this.btn_prodlist_del.Click += new System.EventHandler(this.btn_prodlist_del_Click);
            // 
            // btn_prodlist_add
            // 
            this.btn_prodlist_add.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodlist_add.Location = new System.Drawing.Point(598, 346);
            this.btn_prodlist_add.Name = "btn_prodlist_add";
            this.btn_prodlist_add.Size = new System.Drawing.Size(117, 31);
            this.btn_prodlist_add.TabIndex = 7;
            this.btn_prodlist_add.Text = "Добавить";
            this.btn_prodlist_add.UseVisualStyleBackColor = true;
            this.btn_prodlist_add.Click += new System.EventHandler(this.btn_prodlist_add_Click);
            // 
            // dgv_prodlist
            // 
            this.dgv_prodlist.AllowUserToAddRows = false;
            this.dgv_prodlist.AllowUserToDeleteRows = false;
            this.dgv_prodlist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_prodlist.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_prodlist.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_prodlist.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_prodlist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_prodlist.Location = new System.Drawing.Point(3, 3);
            this.dgv_prodlist.Name = "dgv_prodlist";
            this.dgv_prodlist.ReadOnly = true;
            this.dgv_prodlist.Size = new System.Drawing.Size(962, 323);
            this.dgv_prodlist.TabIndex = 2;
            this.dgv_prodlist.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_prodlist_CellClick);
            // 
            // tab_prodtype
            // 
            this.tab_prodtype.BackColor = System.Drawing.Color.LightGray;
            this.tab_prodtype.Controls.Add(this.label7);
            this.tab_prodtype.Controls.Add(this.tb_typename);
            this.tab_prodtype.Controls.Add(this.btn_prodtype_upd);
            this.tab_prodtype.Controls.Add(this.btn_prodtype_del);
            this.tab_prodtype.Controls.Add(this.btn_prodtype_add);
            this.tab_prodtype.Controls.Add(this.dgv_prodtype);
            this.tab_prodtype.Location = new System.Drawing.Point(4, 22);
            this.tab_prodtype.Name = "tab_prodtype";
            this.tab_prodtype.Padding = new System.Windows.Forms.Padding(3);
            this.tab_prodtype.Size = new System.Drawing.Size(971, 443);
            this.tab_prodtype.TabIndex = 4;
            this.tab_prodtype.Text = "Тип товаров";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(139, 342);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 19);
            this.label7.TabIndex = 14;
            this.label7.Text = "Тип";
            // 
            // tb_typename
            // 
            this.tb_typename.Location = new System.Drawing.Point(181, 341);
            this.tb_typename.Name = "tb_typename";
            this.tb_typename.Size = new System.Drawing.Size(232, 20);
            this.tb_typename.TabIndex = 13;
            // 
            // btn_prodtype_upd
            // 
            this.btn_prodtype_upd.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodtype_upd.Location = new System.Drawing.Point(723, 333);
            this.btn_prodtype_upd.Name = "btn_prodtype_upd";
            this.btn_prodtype_upd.Size = new System.Drawing.Size(117, 31);
            this.btn_prodtype_upd.TabIndex = 12;
            this.btn_prodtype_upd.Text = "Обновить";
            this.btn_prodtype_upd.UseVisualStyleBackColor = true;
            this.btn_prodtype_upd.Click += new System.EventHandler(this.btn_prodtype_upd_Click);
            // 
            // btn_prodtype_del
            // 
            this.btn_prodtype_del.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodtype_del.Location = new System.Drawing.Point(846, 333);
            this.btn_prodtype_del.Name = "btn_prodtype_del";
            this.btn_prodtype_del.Size = new System.Drawing.Size(117, 31);
            this.btn_prodtype_del.TabIndex = 11;
            this.btn_prodtype_del.Text = "Удалить";
            this.btn_prodtype_del.UseVisualStyleBackColor = true;
            this.btn_prodtype_del.Click += new System.EventHandler(this.btn_prodtype_del_Click);
            // 
            // btn_prodtype_add
            // 
            this.btn_prodtype_add.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodtype_add.Location = new System.Drawing.Point(598, 333);
            this.btn_prodtype_add.Name = "btn_prodtype_add";
            this.btn_prodtype_add.Size = new System.Drawing.Size(117, 31);
            this.btn_prodtype_add.TabIndex = 10;
            this.btn_prodtype_add.Text = "Добавить";
            this.btn_prodtype_add.UseVisualStyleBackColor = true;
            this.btn_prodtype_add.Click += new System.EventHandler(this.btn_prodtype_add_Click);
            // 
            // dgv_prodtype
            // 
            this.dgv_prodtype.AllowUserToAddRows = false;
            this.dgv_prodtype.AllowUserToDeleteRows = false;
            this.dgv_prodtype.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_prodtype.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_prodtype.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_prodtype.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_prodtype.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_prodtype.Location = new System.Drawing.Point(6, 3);
            this.dgv_prodtype.Name = "dgv_prodtype";
            this.dgv_prodtype.ReadOnly = true;
            this.dgv_prodtype.Size = new System.Drawing.Size(962, 323);
            this.dgv_prodtype.TabIndex = 2;
            this.dgv_prodtype.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_prodtype_CellClick);
            // 
            // tab_delivery
            // 
            this.tab_delivery.BackColor = System.Drawing.Color.LightGray;
            this.tab_delivery.Controls.Add(this.dgv_delivery);
            this.tab_delivery.Location = new System.Drawing.Point(4, 22);
            this.tab_delivery.Name = "tab_delivery";
            this.tab_delivery.Padding = new System.Windows.Forms.Padding(3);
            this.tab_delivery.Size = new System.Drawing.Size(971, 443);
            this.tab_delivery.TabIndex = 5;
            this.tab_delivery.Text = "Поставки";
            // 
            // dgv_delivery
            // 
            this.dgv_delivery.AllowUserToAddRows = false;
            this.dgv_delivery.AllowUserToDeleteRows = false;
            this.dgv_delivery.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_delivery.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_delivery.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_delivery.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_delivery.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_delivery.Location = new System.Drawing.Point(3, 3);
            this.dgv_delivery.Name = "dgv_delivery";
            this.dgv_delivery.ReadOnly = true;
            this.dgv_delivery.Size = new System.Drawing.Size(962, 323);
            this.dgv_delivery.TabIndex = 2;
            // 
            // tab_orders
            // 
            this.tab_orders.BackColor = System.Drawing.Color.LightGray;
            this.tab_orders.Controls.Add(this.btn_servicectr_add);
            this.tab_orders.Controls.Add(this.btn_addlog);
            this.tab_orders.Controls.Add(this.btn_orders_del);
            this.tab_orders.Controls.Add(this.btn_orders_upd);
            this.tab_orders.Controls.Add(this.btn_orders_add);
            this.tab_orders.Controls.Add(this.dgv_orders);
            this.tab_orders.Location = new System.Drawing.Point(4, 22);
            this.tab_orders.Name = "tab_orders";
            this.tab_orders.Padding = new System.Windows.Forms.Padding(3);
            this.tab_orders.Size = new System.Drawing.Size(971, 443);
            this.tab_orders.TabIndex = 6;
            this.tab_orders.Text = "Заказы";
            // 
            // btn_servicectr_add
            // 
            this.btn_servicectr_add.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_servicectr_add.Location = new System.Drawing.Point(846, 341);
            this.btn_servicectr_add.Name = "btn_servicectr_add";
            this.btn_servicectr_add.Size = new System.Drawing.Size(117, 31);
            this.btn_servicectr_add.TabIndex = 14;
            this.btn_servicectr_add.Text = "Заявка в СЦ";
            this.btn_servicectr_add.UseVisualStyleBackColor = true;
            this.btn_servicectr_add.Click += new System.EventHandler(this.btn_servicectr_add_Click);
            // 
            // btn_addlog
            // 
            this.btn_addlog.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_addlog.Location = new System.Drawing.Point(476, 341);
            this.btn_addlog.Name = "btn_addlog";
            this.btn_addlog.Size = new System.Drawing.Size(117, 31);
            this.btn_addlog.TabIndex = 13;
            this.btn_addlog.Text = "Доставка";
            this.btn_addlog.UseVisualStyleBackColor = true;
            this.btn_addlog.Click += new System.EventHandler(this.btn_addlog_Click);
            // 
            // btn_orders_del
            // 
            this.btn_orders_del.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_orders_del.Location = new System.Drawing.Point(599, 341);
            this.btn_orders_del.Name = "btn_orders_del";
            this.btn_orders_del.Size = new System.Drawing.Size(117, 31);
            this.btn_orders_del.TabIndex = 12;
            this.btn_orders_del.Text = "Отменить";
            this.btn_orders_del.UseVisualStyleBackColor = true;
            this.btn_orders_del.Click += new System.EventHandler(this.btn_orders_del_Click);
            // 
            // btn_orders_upd
            // 
            this.btn_orders_upd.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_orders_upd.Location = new System.Drawing.Point(722, 341);
            this.btn_orders_upd.Name = "btn_orders_upd";
            this.btn_orders_upd.Size = new System.Drawing.Size(117, 31);
            this.btn_orders_upd.TabIndex = 11;
            this.btn_orders_upd.Text = "Оплачено";
            this.btn_orders_upd.UseVisualStyleBackColor = true;
            this.btn_orders_upd.Click += new System.EventHandler(this.btn_orders_upd_Click);
            // 
            // btn_orders_add
            // 
            this.btn_orders_add.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_orders_add.Location = new System.Drawing.Point(353, 341);
            this.btn_orders_add.Name = "btn_orders_add";
            this.btn_orders_add.Size = new System.Drawing.Size(117, 31);
            this.btn_orders_add.TabIndex = 10;
            this.btn_orders_add.Text = "Оформить";
            this.btn_orders_add.UseVisualStyleBackColor = true;
            this.btn_orders_add.Click += new System.EventHandler(this.btn_orders_add_Click);
            // 
            // dgv_orders
            // 
            this.dgv_orders.AllowUserToAddRows = false;
            this.dgv_orders.AllowUserToDeleteRows = false;
            this.dgv_orders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_orders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_orders.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_orders.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_orders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_orders.Location = new System.Drawing.Point(3, 3);
            this.dgv_orders.Name = "dgv_orders";
            this.dgv_orders.ReadOnly = true;
            this.dgv_orders.Size = new System.Drawing.Size(962, 323);
            this.dgv_orders.TabIndex = 2;
            this.dgv_orders.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_orders_CellClick);
            // 
            // tab_serviceord
            // 
            this.tab_serviceord.BackColor = System.Drawing.Color.LightGray;
            this.tab_serviceord.Controls.Add(this.btn_stopped);
            this.tab_serviceord.Controls.Add(this.btn_done);
            this.tab_serviceord.Controls.Add(this.btn_decline);
            this.tab_serviceord.Controls.Add(this.btn_inprogress);
            this.tab_serviceord.Controls.Add(this.dgv_serviceord);
            this.tab_serviceord.Location = new System.Drawing.Point(4, 22);
            this.tab_serviceord.Name = "tab_serviceord";
            this.tab_serviceord.Padding = new System.Windows.Forms.Padding(3);
            this.tab_serviceord.Size = new System.Drawing.Size(971, 443);
            this.tab_serviceord.TabIndex = 7;
            this.tab_serviceord.Text = "Заявки в СЦ";
            // 
            // btn_stopped
            // 
            this.btn_stopped.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_stopped.Location = new System.Drawing.Point(558, 341);
            this.btn_stopped.Name = "btn_stopped";
            this.btn_stopped.Size = new System.Drawing.Size(159, 31);
            this.btn_stopped.TabIndex = 17;
            this.btn_stopped.Text = "Приостановить";
            this.btn_stopped.UseVisualStyleBackColor = true;
            this.btn_stopped.Click += new System.EventHandler(this.btn_stopped_Click);
            // 
            // btn_done
            // 
            this.btn_done.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_done.Location = new System.Drawing.Point(723, 341);
            this.btn_done.Name = "btn_done";
            this.btn_done.Size = new System.Drawing.Size(117, 31);
            this.btn_done.TabIndex = 16;
            this.btn_done.Text = "Выполнено";
            this.btn_done.UseVisualStyleBackColor = true;
            this.btn_done.Click += new System.EventHandler(this.btn_done_Click);
            // 
            // btn_decline
            // 
            this.btn_decline.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_decline.Location = new System.Drawing.Point(846, 341);
            this.btn_decline.Name = "btn_decline";
            this.btn_decline.Size = new System.Drawing.Size(117, 31);
            this.btn_decline.TabIndex = 15;
            this.btn_decline.Text = "Отклонить";
            this.btn_decline.UseVisualStyleBackColor = true;
            this.btn_decline.Click += new System.EventHandler(this.btn_decline_Click);
            // 
            // btn_inprogress
            // 
            this.btn_inprogress.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_inprogress.Location = new System.Drawing.Point(435, 341);
            this.btn_inprogress.Name = "btn_inprogress";
            this.btn_inprogress.Size = new System.Drawing.Size(117, 31);
            this.btn_inprogress.TabIndex = 14;
            this.btn_inprogress.Text = "В работу";
            this.btn_inprogress.UseVisualStyleBackColor = true;
            this.btn_inprogress.Click += new System.EventHandler(this.btn_inprogress_Click);
            // 
            // dgv_serviceord
            // 
            this.dgv_serviceord.AllowUserToAddRows = false;
            this.dgv_serviceord.AllowUserToDeleteRows = false;
            this.dgv_serviceord.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_serviceord.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_serviceord.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_serviceord.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_serviceord.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_serviceord.Location = new System.Drawing.Point(3, 3);
            this.dgv_serviceord.Name = "dgv_serviceord";
            this.dgv_serviceord.ReadOnly = true;
            this.dgv_serviceord.Size = new System.Drawing.Size(962, 323);
            this.dgv_serviceord.TabIndex = 2;
            this.dgv_serviceord.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_serviceord_CellClick);
            // 
            // tab_tnsforder
            // 
            this.tab_tnsforder.BackColor = System.Drawing.Color.LightGray;
            this.tab_tnsforder.Controls.Add(this.dgv_tnsforder);
            this.tab_tnsforder.Location = new System.Drawing.Point(4, 22);
            this.tab_tnsforder.Name = "tab_tnsforder";
            this.tab_tnsforder.Padding = new System.Windows.Forms.Padding(3);
            this.tab_tnsforder.Size = new System.Drawing.Size(971, 443);
            this.tab_tnsforder.TabIndex = 8;
            this.tab_tnsforder.Text = "Заявки на перемещение";
            // 
            // dgv_tnsforder
            // 
            this.dgv_tnsforder.AllowUserToAddRows = false;
            this.dgv_tnsforder.AllowUserToDeleteRows = false;
            this.dgv_tnsforder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_tnsforder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_tnsforder.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_tnsforder.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_tnsforder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_tnsforder.Location = new System.Drawing.Point(3, 3);
            this.dgv_tnsforder.Name = "dgv_tnsforder";
            this.dgv_tnsforder.ReadOnly = true;
            this.dgv_tnsforder.Size = new System.Drawing.Size(962, 323);
            this.dgv_tnsforder.TabIndex = 2;
            // 
            // tab_shops
            // 
            this.tab_shops.BackColor = System.Drawing.Color.LightGray;
            this.tab_shops.Controls.Add(this.dgv_shops);
            this.tab_shops.Location = new System.Drawing.Point(4, 22);
            this.tab_shops.Name = "tab_shops";
            this.tab_shops.Padding = new System.Windows.Forms.Padding(3);
            this.tab_shops.Size = new System.Drawing.Size(971, 443);
            this.tab_shops.TabIndex = 9;
            this.tab_shops.Text = "Магазины";
            // 
            // dgv_shops
            // 
            this.dgv_shops.AllowUserToAddRows = false;
            this.dgv_shops.AllowUserToDeleteRows = false;
            this.dgv_shops.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_shops.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_shops.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_shops.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_shops.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_shops.Location = new System.Drawing.Point(3, 3);
            this.dgv_shops.Name = "dgv_shops";
            this.dgv_shops.ReadOnly = true;
            this.dgv_shops.Size = new System.Drawing.Size(962, 323);
            this.dgv_shops.TabIndex = 2;
            // 
            // tab_servicectr
            // 
            this.tab_servicectr.BackColor = System.Drawing.Color.LightGray;
            this.tab_servicectr.Controls.Add(this.dgv_servicectr);
            this.tab_servicectr.Location = new System.Drawing.Point(4, 22);
            this.tab_servicectr.Name = "tab_servicectr";
            this.tab_servicectr.Padding = new System.Windows.Forms.Padding(3);
            this.tab_servicectr.Size = new System.Drawing.Size(971, 443);
            this.tab_servicectr.TabIndex = 10;
            this.tab_servicectr.Text = "Сервисные центры";
            // 
            // dgv_servicectr
            // 
            this.dgv_servicectr.AllowUserToAddRows = false;
            this.dgv_servicectr.AllowUserToDeleteRows = false;
            this.dgv_servicectr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_servicectr.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_servicectr.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_servicectr.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_servicectr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_servicectr.Location = new System.Drawing.Point(3, 3);
            this.dgv_servicectr.Name = "dgv_servicectr";
            this.dgv_servicectr.ReadOnly = true;
            this.dgv_servicectr.Size = new System.Drawing.Size(962, 323);
            this.dgv_servicectr.TabIndex = 2;
            // 
            // tab_servicelog
            // 
            this.tab_servicelog.BackColor = System.Drawing.Color.LightGray;
            this.tab_servicelog.Controls.Add(this.dgv_logservice);
            this.tab_servicelog.Location = new System.Drawing.Point(4, 22);
            this.tab_servicelog.Name = "tab_servicelog";
            this.tab_servicelog.Padding = new System.Windows.Forms.Padding(3);
            this.tab_servicelog.Size = new System.Drawing.Size(971, 443);
            this.tab_servicelog.TabIndex = 11;
            this.tab_servicelog.Text = "Служба логистики";
            // 
            // dgv_logservice
            // 
            this.dgv_logservice.AllowUserToAddRows = false;
            this.dgv_logservice.AllowUserToDeleteRows = false;
            this.dgv_logservice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_logservice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_logservice.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_logservice.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_logservice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_logservice.Location = new System.Drawing.Point(3, 3);
            this.dgv_logservice.Name = "dgv_logservice";
            this.dgv_logservice.ReadOnly = true;
            this.dgv_logservice.Size = new System.Drawing.Size(962, 323);
            this.dgv_logservice.TabIndex = 2;
            // 
            // tab_offices
            // 
            this.tab_offices.BackColor = System.Drawing.Color.LightGray;
            this.tab_offices.Controls.Add(this.dgv_offices);
            this.tab_offices.Location = new System.Drawing.Point(4, 22);
            this.tab_offices.Name = "tab_offices";
            this.tab_offices.Padding = new System.Windows.Forms.Padding(3);
            this.tab_offices.Size = new System.Drawing.Size(971, 443);
            this.tab_offices.TabIndex = 12;
            this.tab_offices.Text = "Офисы";
            // 
            // dgv_offices
            // 
            this.dgv_offices.AllowUserToAddRows = false;
            this.dgv_offices.AllowUserToDeleteRows = false;
            this.dgv_offices.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_offices.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_offices.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_offices.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_offices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_offices.Location = new System.Drawing.Point(3, 3);
            this.dgv_offices.Name = "dgv_offices";
            this.dgv_offices.ReadOnly = true;
            this.dgv_offices.Size = new System.Drawing.Size(962, 323);
            this.dgv_offices.TabIndex = 2;
            // 
            // tab_employees
            // 
            this.tab_employees.BackColor = System.Drawing.Color.LightGray;
            this.tab_employees.Controls.Add(this.btn_employees_add);
            this.tab_employees.Controls.Add(this.dgv_employees);
            this.tab_employees.Location = new System.Drawing.Point(4, 22);
            this.tab_employees.Name = "tab_employees";
            this.tab_employees.Padding = new System.Windows.Forms.Padding(3);
            this.tab_employees.Size = new System.Drawing.Size(971, 443);
            this.tab_employees.TabIndex = 13;
            this.tab_employees.Text = "Сотрудники";
            // 
            // btn_employees_add
            // 
            this.btn_employees_add.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_employees_add.Location = new System.Drawing.Point(715, 341);
            this.btn_employees_add.Name = "btn_employees_add";
            this.btn_employees_add.Size = new System.Drawing.Size(117, 31);
            this.btn_employees_add.TabIndex = 14;
            this.btn_employees_add.Text = "Добавить";
            this.btn_employees_add.UseVisualStyleBackColor = true;
            this.btn_employees_add.Click += new System.EventHandler(this.btn_employees_add_Click);
            // 
            // dgv_employees
            // 
            this.dgv_employees.AllowUserToAddRows = false;
            this.dgv_employees.AllowUserToDeleteRows = false;
            this.dgv_employees.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_employees.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_employees.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_employees.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_employees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_employees.Location = new System.Drawing.Point(3, 3);
            this.dgv_employees.Name = "dgv_employees";
            this.dgv_employees.ReadOnly = true;
            this.dgv_employees.Size = new System.Drawing.Size(962, 323);
            this.dgv_employees.TabIndex = 2;
            // 
            // tab_positions
            // 
            this.tab_positions.BackColor = System.Drawing.Color.LightGray;
            this.tab_positions.Controls.Add(this.dgv_positions);
            this.tab_positions.Location = new System.Drawing.Point(4, 22);
            this.tab_positions.Name = "tab_positions";
            this.tab_positions.Padding = new System.Windows.Forms.Padding(3);
            this.tab_positions.Size = new System.Drawing.Size(971, 443);
            this.tab_positions.TabIndex = 14;
            this.tab_positions.Text = "Должности";
            // 
            // dgv_positions
            // 
            this.dgv_positions.AllowUserToAddRows = false;
            this.dgv_positions.AllowUserToDeleteRows = false;
            this.dgv_positions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_positions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_positions.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_positions.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_positions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_positions.Location = new System.Drawing.Point(3, 3);
            this.dgv_positions.Name = "dgv_positions";
            this.dgv_positions.ReadOnly = true;
            this.dgv_positions.Size = new System.Drawing.Size(962, 323);
            this.dgv_positions.TabIndex = 2;
            // 
            // tab_suppliers
            // 
            this.tab_suppliers.BackColor = System.Drawing.Color.LightGray;
            this.tab_suppliers.Controls.Add(this.dgv_suppliers);
            this.tab_suppliers.Location = new System.Drawing.Point(4, 22);
            this.tab_suppliers.Name = "tab_suppliers";
            this.tab_suppliers.Padding = new System.Windows.Forms.Padding(3);
            this.tab_suppliers.Size = new System.Drawing.Size(971, 443);
            this.tab_suppliers.TabIndex = 15;
            this.tab_suppliers.Text = "Поставщики";
            // 
            // dgv_suppliers
            // 
            this.dgv_suppliers.AllowUserToAddRows = false;
            this.dgv_suppliers.AllowUserToDeleteRows = false;
            this.dgv_suppliers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_suppliers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_suppliers.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_suppliers.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_suppliers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_suppliers.Location = new System.Drawing.Point(3, 3);
            this.dgv_suppliers.Name = "dgv_suppliers";
            this.dgv_suppliers.ReadOnly = true;
            this.dgv_suppliers.Size = new System.Drawing.Size(962, 323);
            this.dgv_suppliers.TabIndex = 2;
            // 
            // tab_reports
            // 
            this.tab_reports.BackColor = System.Drawing.Color.LightGray;
            this.tab_reports.Controls.Add(this.link_employees);
            this.tab_reports.Controls.Add(this.link_profit);
            this.tab_reports.Location = new System.Drawing.Point(4, 22);
            this.tab_reports.Name = "tab_reports";
            this.tab_reports.Padding = new System.Windows.Forms.Padding(3);
            this.tab_reports.Size = new System.Drawing.Size(971, 443);
            this.tab_reports.TabIndex = 16;
            this.tab_reports.Text = "Отчеты";
            // 
            // link_employees
            // 
            this.link_employees.AutoSize = true;
            this.link_employees.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.link_employees.Location = new System.Drawing.Point(40, 75);
            this.link_employees.Name = "link_employees";
            this.link_employees.Size = new System.Drawing.Size(241, 21);
            this.link_employees.TabIndex = 1;
            this.link_employees.TabStop = true;
            this.link_employees.Text = "Сотрудники и их должности";
            this.link_employees.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_employees_LinkClicked);
            // 
            // link_profit
            // 
            this.link_profit.AutoSize = true;
            this.link_profit.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.link_profit.Location = new System.Drawing.Point(40, 26);
            this.link_profit.Name = "link_profit";
            this.link_profit.Size = new System.Drawing.Size(169, 21);
            this.link_profit.TabIndex = 0;
            this.link_profit.TabStop = true;
            this.link_profit.Text = "Выручка от продаж";
            this.link_profit.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link_profit_LinkClicked);
            // 
            // btn_rep
            // 
            this.btn_rep.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_rep.Location = new System.Drawing.Point(6, 337);
            this.btn_rep.Name = "btn_rep";
            this.btn_rep.Size = new System.Drawing.Size(152, 31);
            this.btn_rep.TabIndex = 4;
            this.btn_rep.Text = "Репликация с ОК";
            this.btn_rep.UseVisualStyleBackColor = true;
            this.btn_rep.Click += new System.EventHandler(this.btn_rep_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 407);
            this.Controls.Add(this.tab_control);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "ЭльДаВидео";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.tab_control.ResumeLayout(false);
            this.tab_prodinstock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodinstock)).EndInit();
            this.tab_admin.ResumeLayout(false);
            this.tab_admin.PerformLayout();
            this.tab_prodinshop.ResumeLayout(false);
            this.tab_prodinshop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodinshop)).EndInit();
            this.tab_prodlist.ResumeLayout(false);
            this.tab_prodlist.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodlist)).EndInit();
            this.tab_prodtype.ResumeLayout(false);
            this.tab_prodtype.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodtype)).EndInit();
            this.tab_delivery.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_delivery)).EndInit();
            this.tab_orders.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_orders)).EndInit();
            this.tab_serviceord.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_serviceord)).EndInit();
            this.tab_tnsforder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tnsforder)).EndInit();
            this.tab_shops.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_shops)).EndInit();
            this.tab_servicectr.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_servicectr)).EndInit();
            this.tab_servicelog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_logservice)).EndInit();
            this.tab_offices.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_offices)).EndInit();
            this.tab_employees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_employees)).EndInit();
            this.tab_positions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_positions)).EndInit();
            this.tab_suppliers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_suppliers)).EndInit();
            this.tab_reports.ResumeLayout(false);
            this.tab_reports.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tab_control;
        private System.Windows.Forms.TabPage tab_prodinstock;
        private System.Windows.Forms.TabPage tab_admin;
        private System.Windows.Forms.Button btn_query;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rtb_logs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtb_query;
        private System.Windows.Forms.DataGridView dgv_prodinstock;
        private System.Windows.Forms.Button btn_prodinstock_del;
        private System.Windows.Forms.Button btn_prodinstock_add;
        private System.Windows.Forms.TabPage tab_prodinshop;
        private System.Windows.Forms.TabPage tab_prodlist;
        private System.Windows.Forms.TabPage tab_prodtype;
        private System.Windows.Forms.TabPage tab_delivery;
        private System.Windows.Forms.TabPage tab_orders;
        private System.Windows.Forms.TabPage tab_serviceord;
        private System.Windows.Forms.TabPage tab_tnsforder;
        private System.Windows.Forms.TabPage tab_shops;
        private System.Windows.Forms.TabPage tab_servicectr;
        private System.Windows.Forms.TabPage tab_servicelog;
        private System.Windows.Forms.TabPage tab_offices;
        private System.Windows.Forms.TabPage tab_employees;
        private System.Windows.Forms.TabPage tab_positions;
        private System.Windows.Forms.TabPage tab_suppliers;
        private System.Windows.Forms.DataGridView dgv_prodinshop;
        private System.Windows.Forms.DataGridView dgv_prodlist;
        private System.Windows.Forms.DataGridView dgv_prodtype;
        private System.Windows.Forms.DataGridView dgv_delivery;
        private System.Windows.Forms.DataGridView dgv_orders;
        private System.Windows.Forms.DataGridView dgv_serviceord;
        private System.Windows.Forms.DataGridView dgv_tnsforder;
        private System.Windows.Forms.DataGridView dgv_shops;
        private System.Windows.Forms.DataGridView dgv_servicectr;
        private System.Windows.Forms.DataGridView dgv_logservice;
        private System.Windows.Forms.DataGridView dgv_offices;
        private System.Windows.Forms.DataGridView dgv_employees;
        private System.Windows.Forms.DataGridView dgv_positions;
        private System.Windows.Forms.DataGridView dgv_suppliers;
        private System.Windows.Forms.Button btn_prodinshop_upd;
        private System.Windows.Forms.Button btn_prodinshop_del;
        private System.Windows.Forms.Button btn_prodinshop_add;
        private System.Windows.Forms.TextBox tb_war;
        private System.Windows.Forms.TextBox tb_price;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_prodlist_upd;
        private System.Windows.Forms.Button btn_prodlist_del;
        private System.Windows.Forms.Button btn_prodlist_add;
        private System.Windows.Forms.TextBox tb_prodlist_man;
        private System.Windows.Forms.TextBox tb_prodlist_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_typename;
        private System.Windows.Forms.Button btn_prodtype_upd;
        private System.Windows.Forms.Button btn_prodtype_del;
        private System.Windows.Forms.Button btn_prodtype_add;
        private System.Windows.Forms.Button btn_orders_del;
        private System.Windows.Forms.Button btn_orders_upd;
        private System.Windows.Forms.Button btn_orders_add;
        private System.Windows.Forms.Button btn_addlog;
        private System.Windows.Forms.Button btn_servicectr_add;
        private System.Windows.Forms.Button btn_stopped;
        private System.Windows.Forms.Button btn_done;
        private System.Windows.Forms.Button btn_decline;
        private System.Windows.Forms.Button btn_inprogress;
        private System.Windows.Forms.TabPage tab_reports;
        private System.Windows.Forms.LinkLabel link_profit;
        private System.Windows.Forms.LinkLabel link_employees;
        private System.Windows.Forms.Button btn_employees_add;
        private System.Windows.Forms.Button btn_rep;
    }
}

