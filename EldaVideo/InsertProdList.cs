﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EldaVideo
{
    public partial class InsertProdList : MySQLForm
    {
        private string name;
        private string manufacturer;
        private int typeID;

        public InsertProdList(MySqlConnection _mySqlConnect)
        {
            InitializeComponent();
            mySqlConnect = _mySqlConnect;
            dataSet = new DataSet();
            dataSet.Tables.Add("PRODTYPE");
            dgv_prodtype.DataSource = dataSet.Tables["PRODTYPE"];
            typeID = -1;
            refresh_prodtype();
        }

        private void refresh_prodtype()
        {
            try
            {
                query = @"SELECT PRODTYPE.ID AS Идентификатор, PRODTYPE.Name as Тип FROM PRODTYPE";
                renew_data_table("PRODTYPE", query);
                dgv_prodtype.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        public void getParameters(out int _typeID, out string _name, out string _manufacturer)
        {
            _typeID = typeID;
            _name = name;
            _manufacturer = manufacturer;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dgv_prodtype_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_prodtype.Rows.Count)
                    return;
                if (dgv_prodtype.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                typeID = Convert.ToInt32(dgv_prodtype.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        private void btn_prodlist_add_Click(object sender, EventArgs e)
        {
            try
            {
                if (typeID == -1)
                {
                    MessageBox.Show("Нужно выбрать тип товара из списка");
                    return;
                }
                name = tb_name.Text;
                manufacturer = tb_man.Text;
                this.DialogResult = DialogResult.Yes;
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Указаны не все параметры: " + _ex.Message.ToString());
            }
        }
    }
}
