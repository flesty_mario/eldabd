﻿namespace EldaVideo
{
    partial class PurchaseOrder
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_prodinshop = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_purchase = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodinshop)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_prodinshop
            // 
            this.dgv_prodinshop.AllowUserToAddRows = false;
            this.dgv_prodinshop.AllowUserToDeleteRows = false;
            this.dgv_prodinshop.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_prodinshop.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_prodinshop.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_prodinshop.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_prodinshop.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_prodinshop.Location = new System.Drawing.Point(12, 31);
            this.dgv_prodinshop.Name = "dgv_prodinshop";
            this.dgv_prodinshop.Size = new System.Drawing.Size(820, 230);
            this.dgv_prodinshop.TabIndex = 2;
            this.dgv_prodinshop.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_prodinshop_CellClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 19);
            this.label4.TabIndex = 9;
            this.label4.Text = "Товар в магазине:";
            // 
            // btn_purchase
            // 
            this.btn_purchase.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_purchase.Location = new System.Drawing.Point(369, 287);
            this.btn_purchase.Name = "btn_purchase";
            this.btn_purchase.Size = new System.Drawing.Size(117, 31);
            this.btn_purchase.TabIndex = 10;
            this.btn_purchase.Text = "Оформить";
            this.btn_purchase.UseVisualStyleBackColor = true;
            this.btn_purchase.Click += new System.EventHandler(this.btn_purchase_Click);
            // 
            // PurchaseOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 330);
            this.Controls.Add(this.btn_purchase);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgv_prodinshop);
            this.Name = "PurchaseOrder";
            this.Text = "Оформление заказа";
            this.Load += new System.EventHandler(this.PurchaseOrder_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodinshop)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_prodinshop;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_purchase;
    }
}