﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EldaVideo
{
    public partial class ShowReport : MySQLForm
    {
        public ShowReport(MySqlConnection _mySqlConnect, string _query)
        {
            InitializeComponent();
            mySqlConnect = _mySqlConnect;
            query = _query;
            dataSet = new DataSet();
            dataSet.Tables.Add("REPORT");
            dgv_report.DataSource = dataSet.Tables["REPORT"];
            renew_data_table("REPORT", query);
            dgv_report.Refresh();
        }
    }
}
