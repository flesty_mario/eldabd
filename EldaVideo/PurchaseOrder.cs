﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EldaVideo
{
    public partial class PurchaseOrder : MySQLForm
    {
        private int productID;
        private int posID;
        private double price;
        public PurchaseOrder(MySqlConnection _mySqlConnect)
        {
            InitializeComponent();
            mySqlConnect = _mySqlConnect;
            productID = -1;
            dataSet = new DataSet();
            dataSet.Tables.Add("PRODINSHOP");
            dgv_prodinshop.DataSource = dataSet.Tables["PRODINSHOP"];
            refresh_prodinshop();
        }

        private void PurchaseOrder_Load(object sender, EventArgs e)
        {

        }

        private void btn_purchase_Click(object sender, EventArgs e)
        {
            if (productID == -1)
            {
                MessageBox.Show("Нужно выбрать товар");
            }
            else
            {
                try
                {
                    query = @"DELETE FROM Prodinshop WHERE ID=@productID";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@productId", posID);
                    mySqlCommand.ExecuteNonQuery();
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception _ex)
                {
                    MessageBox.Show("Ошибка удаления данных: " + _ex.Message.ToString());
                }
            }
        }

        public void get_parameters(out int _productID, out double _price)
        {
            _productID = productID;
            _price = price;
        }

        private void refresh_prodinshop()
        {
            try
            {
                query = @"SELECT PRODINSHOP.Product AS ID_Товара, PRODINSHOP.ID AS ID_позиции, PRODLIST.Name as Товар, PRODTYPE.Name as Тип, PRODINSHOP.Price as Цена 
                          FROM PRODINSHOP, PRODLIST, PRODTYPE
                          WHERE PRODINSHOP.Product=PRODLIST.ID AND PRODLIST.Type=PRODTYPE.ID;";
                renew_data_table("PRODINSHOP", query);
                dgv_prodinshop.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void dgv_prodinshop_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_prodinshop.Rows.Count)
                    return;
                if (dgv_prodinshop.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                productID = Convert.ToInt32(dgv_prodinshop.Rows[e.RowIndex].Cells[0].Value);
                posID = Convert.ToInt32(dgv_prodinshop.Rows[e.RowIndex].Cells[1].Value);
                price = Convert.ToDouble(dgv_prodinshop.Rows[e.RowIndex].Cells[4].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }
    }
}
