﻿namespace EldaVideo
{
    partial class AddServiceCtr
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_servicectr = new System.Windows.Forms.DataGridView();
            this.btn_apply = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_servicectr)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_servicectr
            // 
            this.dgv_servicectr.AllowUserToAddRows = false;
            this.dgv_servicectr.AllowUserToDeleteRows = false;
            this.dgv_servicectr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_servicectr.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_servicectr.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_servicectr.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_servicectr.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_servicectr.Location = new System.Drawing.Point(1, 2);
            this.dgv_servicectr.Name = "dgv_servicectr";
            this.dgv_servicectr.Size = new System.Drawing.Size(523, 194);
            this.dgv_servicectr.TabIndex = 4;
            this.dgv_servicectr.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_servicectr_CellClick);
            // 
            // btn_apply
            // 
            this.btn_apply.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_apply.Location = new System.Drawing.Point(196, 211);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(117, 31);
            this.btn_apply.TabIndex = 12;
            this.btn_apply.Text = "Выбрать";
            this.btn_apply.UseVisualStyleBackColor = true;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // AddServiceCtr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 254);
            this.Controls.Add(this.btn_apply);
            this.Controls.Add(this.dgv_servicectr);
            this.Name = "AddServiceCtr";
            this.Text = "AddServiceCtr";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_servicectr)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_servicectr;
        private System.Windows.Forms.Button btn_apply;
    }
}