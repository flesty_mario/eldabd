﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EldaVideo
{
    public partial class MySQLForm : Form
    {
        protected MySqlConnection mySqlConnect;
        protected MySqlConnection mySqlConnectHost;
        protected MySqlCommand mySqlCommand;
        protected MySqlDataAdapter mySqlDataAdapter;
        protected MySqlDataReader mySqlDataReader;
        protected DataSet dataSet;

        protected string query;

        public MySQLForm()
        {
            mySqlConnect = null;
            mySqlConnectHost = null;
            mySqlCommand = null;
            mySqlDataAdapter = null;
            mySqlDataReader = null;
            dataSet = null;
        }

        protected void renew_data_table(string tableName, string querySelect)
        {
            dataSet.Tables[tableName].Clear();
            mySqlCommand = new MySqlCommand(querySelect, mySqlConnect);
            mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
            mySqlDataAdapter.Fill(dataSet.Tables[tableName]);
        }

        protected void renew_data_table_from_host(string tableName, string querySelect)
        {
            dataSet.Tables[tableName].Clear();
            mySqlCommand = new MySqlCommand(querySelect, mySqlConnectHost);
            mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand);
            mySqlDataAdapter.Fill(dataSet.Tables[tableName]);
        }
    }
}
