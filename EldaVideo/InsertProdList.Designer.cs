﻿namespace EldaVideo
{
    partial class InsertProdList
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_name = new System.Windows.Forms.TextBox();
            this.tb_man = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgv_prodtype = new System.Windows.Forms.DataGridView();
            this.btn_prodlist_add = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodtype)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(172, 22);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(267, 20);
            this.tb_name.TabIndex = 0;
            // 
            // tb_man
            // 
            this.tb_man.Location = new System.Drawing.Point(172, 69);
            this.tb_man.Name = "tb_man";
            this.tb_man.Size = new System.Drawing.Size(267, 20);
            this.tb_man.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Название";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Производитель";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "Тип";
            // 
            // dgv_prodtype
            // 
            this.dgv_prodtype.AllowUserToAddRows = false;
            this.dgv_prodtype.AllowUserToDeleteRows = false;
            this.dgv_prodtype.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_prodtype.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_prodtype.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_prodtype.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_prodtype.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_prodtype.Location = new System.Drawing.Point(16, 133);
            this.dgv_prodtype.Name = "dgv_prodtype";
            this.dgv_prodtype.Size = new System.Drawing.Size(639, 179);
            this.dgv_prodtype.TabIndex = 5;
            this.dgv_prodtype.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_prodtype_CellClick);
            // 
            // btn_prodlist_add
            // 
            this.btn_prodlist_add.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_prodlist_add.Location = new System.Drawing.Point(281, 322);
            this.btn_prodlist_add.Name = "btn_prodlist_add";
            this.btn_prodlist_add.Size = new System.Drawing.Size(117, 31);
            this.btn_prodlist_add.TabIndex = 8;
            this.btn_prodlist_add.Text = "Добавить";
            this.btn_prodlist_add.UseVisualStyleBackColor = true;
            this.btn_prodlist_add.Click += new System.EventHandler(this.btn_prodlist_add_Click);
            // 
            // InsertProdList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 365);
            this.Controls.Add(this.btn_prodlist_add);
            this.Controls.Add(this.dgv_prodtype);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_man);
            this.Controls.Add(this.tb_name);
            this.Name = "InsertProdList";
            this.Text = "InsertProdList";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodtype)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.TextBox tb_man;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgv_prodtype;
        private System.Windows.Forms.Button btn_prodlist_add;
    }
}