﻿namespace EldaVideo
{
    partial class InsertProdInStock
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_addinprod = new System.Windows.Forms.DataGridView();
            this.btn_addprodinstock = new System.Windows.Forms.Button();
            this.tab_prodinstock = new System.Windows.Forms.TabControl();
            this.tab_add = new System.Windows.Forms.TabPage();
            this.tab_delete = new System.Windows.Forms.TabPage();
            this.dgv_prodinstock = new System.Windows.Forms.DataGridView();
            this.btn_del_ok = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_addinprod)).BeginInit();
            this.tab_prodinstock.SuspendLayout();
            this.tab_add.SuspendLayout();
            this.tab_delete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodinstock)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_addinprod
            // 
            this.dgv_addinprod.AllowUserToAddRows = false;
            this.dgv_addinprod.AllowUserToDeleteRows = false;
            this.dgv_addinprod.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_addinprod.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_addinprod.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_addinprod.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_addinprod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_addinprod.Location = new System.Drawing.Point(6, 6);
            this.dgv_addinprod.Name = "dgv_addinprod";
            this.dgv_addinprod.Size = new System.Drawing.Size(921, 311);
            this.dgv_addinprod.TabIndex = 0;
            this.dgv_addinprod.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_addinprod_CellClick);
            // 
            // btn_addprodinstock
            // 
            this.btn_addprodinstock.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_addprodinstock.Location = new System.Drawing.Point(401, 323);
            this.btn_addprodinstock.Name = "btn_addprodinstock";
            this.btn_addprodinstock.Size = new System.Drawing.Size(140, 33);
            this.btn_addprodinstock.TabIndex = 1;
            this.btn_addprodinstock.Text = "Добавить товар";
            this.btn_addprodinstock.UseVisualStyleBackColor = true;
            this.btn_addprodinstock.Click += new System.EventHandler(this.btn_addprodinstock_Click);
            // 
            // tab_prodinstock
            // 
            this.tab_prodinstock.Controls.Add(this.tab_add);
            this.tab_prodinstock.Controls.Add(this.tab_delete);
            this.tab_prodinstock.Location = new System.Drawing.Point(12, 12);
            this.tab_prodinstock.Name = "tab_prodinstock";
            this.tab_prodinstock.SelectedIndex = 0;
            this.tab_prodinstock.Size = new System.Drawing.Size(941, 388);
            this.tab_prodinstock.TabIndex = 2;
            // 
            // tab_add
            // 
            this.tab_add.Controls.Add(this.dgv_addinprod);
            this.tab_add.Controls.Add(this.btn_addprodinstock);
            this.tab_add.Location = new System.Drawing.Point(4, 22);
            this.tab_add.Name = "tab_add";
            this.tab_add.Padding = new System.Windows.Forms.Padding(3);
            this.tab_add.Size = new System.Drawing.Size(933, 362);
            this.tab_add.TabIndex = 0;
            this.tab_add.Text = "Добавление элемента";
            this.tab_add.UseVisualStyleBackColor = true;
            // 
            // tab_delete
            // 
            this.tab_delete.Controls.Add(this.btn_del_ok);
            this.tab_delete.Controls.Add(this.dgv_prodinstock);
            this.tab_delete.Location = new System.Drawing.Point(4, 22);
            this.tab_delete.Name = "tab_delete";
            this.tab_delete.Padding = new System.Windows.Forms.Padding(3);
            this.tab_delete.Size = new System.Drawing.Size(933, 362);
            this.tab_delete.TabIndex = 1;
            this.tab_delete.Text = "Удаление";
            this.tab_delete.UseVisualStyleBackColor = true;
            // 
            // dgv_prodinstock
            // 
            this.dgv_prodinstock.AllowUserToAddRows = false;
            this.dgv_prodinstock.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_prodinstock.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_prodinstock.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_prodinstock.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_prodinstock.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_prodinstock.Location = new System.Drawing.Point(6, 6);
            this.dgv_prodinstock.Name = "dgv_prodinstock";
            this.dgv_prodinstock.Size = new System.Drawing.Size(921, 306);
            this.dgv_prodinstock.TabIndex = 0;
            this.dgv_prodinstock.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_prodinstock_CellClick);
            // 
            // btn_del_ok
            // 
            this.btn_del_ok.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_del_ok.Location = new System.Drawing.Point(410, 318);
            this.btn_del_ok.Name = "btn_del_ok";
            this.btn_del_ok.Size = new System.Drawing.Size(122, 38);
            this.btn_del_ok.TabIndex = 1;
            this.btn_del_ok.Text = "Удалить";
            this.btn_del_ok.UseVisualStyleBackColor = true;
            this.btn_del_ok.Click += new System.EventHandler(this.btn_del_ok_Click);
            // 
            // InsertProdInStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(954, 403);
            this.Controls.Add(this.tab_prodinstock);
            this.Name = "InsertProdInStock";
            this.Text = "Добавление товара";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_addinprod)).EndInit();
            this.tab_prodinstock.ResumeLayout(false);
            this.tab_add.ResumeLayout(false);
            this.tab_delete.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_prodinstock)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_addinprod;
        private System.Windows.Forms.Button btn_addprodinstock;
        private System.Windows.Forms.TabControl tab_prodinstock;
        private System.Windows.Forms.TabPage tab_add;
        private System.Windows.Forms.TabPage tab_delete;
        private System.Windows.Forms.DataGridView dgv_prodinstock;
        private System.Windows.Forms.Button btn_del_ok;
    }
}