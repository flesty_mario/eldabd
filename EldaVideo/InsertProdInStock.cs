﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EldaVideo
{
    public partial class InsertProdInStock : MySQLForm
    {
        private int productID;

        public InsertProdInStock(MySqlConnection _mySqlConnect, int action)
        {
            InitializeComponent();
            mySqlConnect = _mySqlConnect;
            productID = -1;
            dataSet = new DataSet();
            dataSet.Tables.Add("PRODLIST");
            dataSet.Tables.Add("PRODINSTOCK");
            dgv_addinprod.DataSource = dataSet.Tables["PRODLIST"];
            dgv_prodinstock.DataSource = dataSet.Tables["PRODINSTOCK"];
            switch (action)
            {
                case 1:
                    tab_delete.Parent = null;
                    refresh_prodlist();
                    break;
                case 2:
                    tab_add.Parent = null;
                    refresh_prodinstock();
                    break;
            }
        }

        public int getSelectedProd()
        {
            return productID;
        }

        private void dgv_addinprod_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_addinprod.Rows.Count)
                    return;
                if (dgv_addinprod.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                productID = Convert.ToInt32(dgv_addinprod.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        private void refresh_prodlist()
        {
            try
            {
                query = @"SELECT PRODLIST.ID, PRODLIST.Name AS ProductName, PRODTYPE.Name AS ProductType 
                                 FROM PRODLIST, PRODTYPE
                                 WHERE PRODLIST.Type=PRODTYPE.ID;";
                renew_data_table("PRODLIST", query);
                dgv_addinprod.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_prodinstock()
        {
            try
            {
                query = @"SELECT PRODINSTOCK.ID, PRODLIST.Name AS ProductName, PRODTYPE.Name AS ProductType 
                                 FROM PRODINSTOCK, PRODLIST, PRODTYPE
                                 WHERE PRODINSTOCK.Product=PRODLIST.ID AND PRODLIST.Type=PRODTYPE.ID;";
                renew_data_table("PRODINSTOCK", query);
                dgv_prodinstock.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_addprodinstock_Click(object sender, EventArgs e)
        {
            if (productID != -1)
            {
                this.DialogResult = DialogResult.Yes;
            }
            else
            {
                MessageBox.Show("Сначала нужно выбрать товар из списка");
            }
        }

        private void dgv_prodinstock_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_prodinstock.Rows.Count)
                    return;
                if (dgv_prodinstock.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                productID = Convert.ToInt32(dgv_prodinstock.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        private void btn_del_ok_Click(object sender, EventArgs e)
        {
            try
            {
                if (productID != -1)
                {
                    query = @"DELETE 
                              FROM PRODINSTOCK
                              WHERE PRODINSTOCK.ID=@productID";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@productID", productID);
                    mySqlCommand.ExecuteNonQuery();
                    refresh_prodinstock();
                }
                else
                {
                    MessageBox.Show("Сначала нужно выбрать товар из списка");
                }
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }
    }
}
