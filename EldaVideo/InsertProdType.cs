﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EldaVideo
{
    public partial class InsertProdType : Form
    {
        private string name;

        public InsertProdType()
        {
            InitializeComponent();
        }

        private void btn_apply_Click(object sender, EventArgs e)
        {
            if (tb_typename.Text != "")
            {
                name = tb_typename.Text;
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("Необходимо указать название типа");
            }
        }

        public string get_name()
        {
            return name;
        }
    }
}
