﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EldaVideo
{
    public partial class Prodinshop : MySQLForm
    {
        private double price;
        private double warranty;
        private int productID;
        private int shopID;

        public Prodinshop(MySqlConnection _mySqlConnect)
        {
            InitializeComponent();
            mySqlConnect = _mySqlConnect;
            dataSet = new DataSet();
            dataSet.Tables.Add("TNSFORDER");
            dgv_tnsforder.DataSource = dataSet.Tables["TNSFORDER"];
            productID = -1;
            refresh_tnsforders();
        }

        private void refresh_tnsforders()
        {
            try
            {
                query = @"SELECT TNSFORDER.ID AS Номер_заявки, PRODLIST.ID AS ID_товара, PRODLIST.NAME AS Товар, SHOPS.ID AS ID_пункта_назначения, SHOPS.ADDRESS AS Пункт_назначения, LOGSERVICE.NAME AS Служба_доставки
                          FROM TNSFORDER, PRODINSTOCK, PRODLIST, SHOPS, LOGSERVICE
                          WHERE TNSFORDER.Product=PRODINSTOCK.ID AND PRODINSTOCK.Product=PRODLIST.ID AND TNSFORDER.SHOP=SHOPS.ID AND TNSFORDER.LOGSERVICE=LOGSERVICE.ID;";
                renew_data_table("TNSFORDER", query);
                dgv_tnsforder.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_prodinshop_apply_Click(object sender, EventArgs e)
        {
            try
            {
                if (productID == -1)
                {
                    MessageBox.Show("Нужно выбрать товар из списка");
                    return;
                }
                price = Convert.ToInt32(tb_price.Text);
                warranty = Convert.ToInt32(tb_war.Text);
                this.DialogResult = DialogResult.Yes;
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Указаны не все параметры: " + _ex.Message.ToString());
            }
        }

        public void getParameters(out int _prodID, out double _price, out double _warranty, out int _shopID)
        {
            _prodID = productID;
            _price = price;
            _warranty = warranty;
            _shopID = shopID;
        }

        private void dgv_tnsforder_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_tnsforder.Rows.Count)
                    return;
                if (dgv_tnsforder.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                productID = Convert.ToInt32(dgv_tnsforder.Rows[e.RowIndex].Cells[1].Value);
                shopID = Convert.ToInt32(dgv_tnsforder.Rows[e.RowIndex].Cells[3].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }
    }
}
