﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EldaVideo
{
    public partial class AddLogService : MySQLForm
    {
        private int logserviceID;
        public AddLogService(MySqlConnection _mySqlConnect)
        {
            InitializeComponent();
            mySqlConnect = _mySqlConnect;
            logserviceID = -1;
            dataSet = new DataSet();
            dataSet.Tables.Add("LOGSERVICE");
            dgv_logservice.DataSource = dataSet.Tables["LOGSERVICE"];
            refresh_logservice();
        }

        private void refresh_logservice()
        {
            try
            {
                query = @"SELECT * FROM LOGSERVICE;";
                renew_data_table("LOGSERVICE", query);
                dgv_logservice.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_apply_Click(object sender, EventArgs e)
        {
            if (logserviceID == -1)
            {
                MessageBox.Show("Нужно выбрать службу доставки");
            }
            else
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void dgv_logservice_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_logservice.Rows.Count)
                    return;
                if (dgv_logservice.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                logserviceID = Convert.ToInt32(dgv_logservice.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        public int get_logserviceID()
        {
            return logserviceID;
        }
    }
}
