﻿namespace EldaVideo
{
    partial class AddLogService
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_logservice = new System.Windows.Forms.DataGridView();
            this.btn_apply = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_logservice)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_logservice
            // 
            this.dgv_logservice.AllowUserToAddRows = false;
            this.dgv_logservice.AllowUserToDeleteRows = false;
            this.dgv_logservice.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_logservice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_logservice.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_logservice.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_logservice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_logservice.Location = new System.Drawing.Point(2, 2);
            this.dgv_logservice.Name = "dgv_logservice";
            this.dgv_logservice.Size = new System.Drawing.Size(524, 197);
            this.dgv_logservice.TabIndex = 3;
            this.dgv_logservice.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_logservice_CellClick);
            // 
            // btn_apply
            // 
            this.btn_apply.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_apply.Location = new System.Drawing.Point(209, 219);
            this.btn_apply.Name = "btn_apply";
            this.btn_apply.Size = new System.Drawing.Size(117, 31);
            this.btn_apply.TabIndex = 11;
            this.btn_apply.Text = "Выбрать";
            this.btn_apply.UseVisualStyleBackColor = true;
            this.btn_apply.Click += new System.EventHandler(this.btn_apply_Click);
            // 
            // AddLogService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 262);
            this.Controls.Add(this.btn_apply);
            this.Controls.Add(this.dgv_logservice);
            this.Name = "AddLogService";
            this.Text = "Выбрать службу доставки";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_logservice)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_logservice;
        private System.Windows.Forms.Button btn_apply;
    }
}