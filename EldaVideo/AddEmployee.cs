﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EldaVideo
{
    public partial class AddEmployee : MySQLForm
    {
        private int positionID;

        public AddEmployee(MySqlConnection _mySqlConnect)
        {
            InitializeComponent();
            mySqlConnect = _mySqlConnect;
            dataSet = new DataSet();
            dataSet.Tables.Add("POSITIONS");
            dgv_positions.DataSource = dataSet.Tables["POSITIONS"];
            positionID = -1;
            refresh_positions();
        }

        private void AddEmployee_Load(object sender, EventArgs e)
        {

        }

        private void refresh_positions()
        {
            try
            {
                query = @"SELECT * FROM POSITIONS;";
                renew_data_table("POSITIONS", query);
                dgv_positions.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void dgv_positions_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_positions.Rows.Count)
                    return;
                if (dgv_positions.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                positionID = Convert.ToInt32(dgv_positions.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        private void btn_apply_Click(object sender, EventArgs e)
        {
            try
            {
                if (positionID != -1)
                {
                    query = @"INSERT INTO EMPLOYEES(SURNAME,NAME,MIDDLENAME,BIRTHDATE,POSITION,LOGSERVICE,SHOPS,SERVICECTR,OFFICE)
                          VALUES(@Surname, @Name, @MiddleName,@BirthDate,@Position,@Logservice,@Shops,@Servicectr,@Office)";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@Surname", tb_surname.Text);
                    mySqlCommand.Parameters.AddWithValue("@Name", tb_name.Text);
                    mySqlCommand.Parameters.AddWithValue("@MiddleName", tb_middlename.Text);
                    mySqlCommand.Parameters.AddWithValue("@BirthDate", dt_birthdate.Value);
                    mySqlCommand.Parameters.AddWithValue("@Position", positionID);
                    mySqlCommand.Parameters.AddWithValue("@Logservice", null);
                    mySqlCommand.Parameters.AddWithValue("@Shops", null);
                    mySqlCommand.Parameters.AddWithValue("@Servicectr", null);
                    mySqlCommand.Parameters.AddWithValue("@Office", null);
                    mySqlCommand.ExecuteNonQuery();
                    this.DialogResult = DialogResult.Yes;
                }
                else
                {
                    MessageBox.Show("Нужно указать должность");
                }
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка добавления данных: " + _ex.Message.ToString());
            }
        }
    }
}
