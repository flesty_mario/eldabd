﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EldaVideo
{
    public partial class AddServiceCtr : MySQLForm
    {
        private int servicectrID;
        public AddServiceCtr(MySqlConnection _mySqlConnect)
        {
            InitializeComponent();
            mySqlConnect = _mySqlConnect;
            servicectrID = -1;
            dataSet = new DataSet();
            dataSet.Tables.Add("SERVICECTR");
            dgv_servicectr.DataSource = dataSet.Tables["SERVICECTR"];
            refresh_servicectr();
        }

        private void refresh_servicectr()
        {
            try
            {
                query = @"SELECT * FROM SERVICECTR;";
                renew_data_table("SERVICECTR", query);
                dgv_servicectr.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void dgv_servicectr_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_servicectr.Rows.Count)
                    return;
                if (dgv_servicectr.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                servicectrID = Convert.ToInt32(dgv_servicectr.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        public int get_servicectrID()
        {
            return servicectrID;
        }

        private void btn_apply_Click(object sender, EventArgs e)
        {
            if (servicectrID == -1)
            {
                MessageBox.Show("Нужно выбрать товар");
            }
            else
            {
                try
                {
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception _ex)
                {
                    MessageBox.Show("Ошибка удаления данных: " + _ex.Message.ToString());
                }
            }
        }
    }
}
