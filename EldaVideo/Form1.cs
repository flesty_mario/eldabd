﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.IO;

namespace EldaVideo
{
    public partial class Form1 : MySQLForm
    {
        private string roleName;
        private int prodinshopID;
        private int prodlistID;
        private int prodtypeID;
        private int orderID;
        private int serviceordID;

        public Form1()
        {
            AuthorityMatrix.setMatrix();
            Login _dlg = new Login();
            _dlg.ShowDialog();
            if (_dlg.DialogResult == DialogResult.Yes)
            {
                InitializeComponent();
                mySqlConnect = _dlg.getConnection();
                mySqlConnectHost = _dlg.getConnectionHost();
                roleName = _dlg.getUserName();

                authorization();

                dataSet = new DataSet();
                dataSet.Tables.Add("PRODINSTOCK");
                dataSet.Tables.Add("PRODLIST");
                dataSet.Tables.Add("PRODINSHOP");
                dataSet.Tables.Add("PRODTYPE");
                dataSet.Tables.Add("EMPLOYEES");
                dataSet.Tables.Add("OFFICES");
                dataSet.Tables.Add("SHOPS");
                dataSet.Tables.Add("LOGSERVICES");
                dataSet.Tables.Add("SERVICECTR");
                dataSet.Tables.Add("ORDERS");
                dataSet.Tables.Add("SERVICEORD");
                dataSet.Tables.Add("POSITIONS");
                dataSet.Tables.Add("DELIVERY");
                dataSet.Tables.Add("SUPPLIERS");
                dataSet.Tables.Add("TNSFORDER");

                dgv_prodinstock.DataSource = dataSet.Tables["PRODINSTOCK"];
                dgv_prodinshop.DataSource = dataSet.Tables["PRODINSHOP"];
                dgv_delivery.DataSource = dataSet.Tables["DELIVERY"];
                dgv_prodlist.DataSource = dataSet.Tables["PRODLIST"];
                dgv_employees.DataSource = dataSet.Tables["EMPLOYEES"];
                dgv_prodtype.DataSource = dataSet.Tables["PRODTYPE"];
                dgv_positions.DataSource = dataSet.Tables["POSITIONS"];
                dgv_orders.DataSource = dataSet.Tables["ORDERS"];
                dgv_tnsforder.DataSource = dataSet.Tables["TNSFORDER"];
                dgv_serviceord.DataSource = dataSet.Tables["SERVICEORD"];
                dgv_servicectr.DataSource = dataSet.Tables["SERVICECTR"];
                dgv_logservice.DataSource = dataSet.Tables["LOGSERVICES"];
                dgv_suppliers.DataSource = dataSet.Tables["SUPPLIERS"];
                dgv_shops.DataSource = dataSet.Tables["SHOPS"];
                dgv_offices.DataSource = dataSet.Tables["OFFICES"];

                if (tab_prodinstock.Parent != null) refresh_prodinstock();
                if (tab_delivery.Parent != null) refresh_delivery();
                if (tab_servicelog.Parent != null) refresh_logservice();
                if (tab_prodlist.Parent != null) refresh_prodlist();
                if (tab_prodinshop.Parent != null) refresh_prodinshop();
                if (tab_employees.Parent != null) refresh_employees();
                if (tab_positions.Parent != null) refresh_positions();
                if (tab_prodtype.Parent != null) refresh_prodtype();
                if (tab_offices.Parent != null) refresh_offices();
                if (tab_shops.Parent != null) refresh_shops();
                if (tab_tnsforder.Parent != null) refresh_tnsforders();
                if (tab_serviceord.Parent != null) refresh_serviceord();
                if (tab_servicectr.Parent != null) refresh_servicectr();
                if (tab_suppliers.Parent != null) refresh_suppliers();
                if (tab_orders.Parent != null) refresh_orders();
            }
        }

        private void authorization()
        {
            tab_admin.Parent = AuthorityMatrix.authorityMatrix[roleName]["CONSOLE"].Equals("SDIU") ? tab_control : null;
            tab_prodinstock.Parent = AuthorityMatrix.authorityMatrix[roleName]["PRODINSTOCK"].Contains('S') ? tab_control : null;
            tab_delivery.Parent = AuthorityMatrix.authorityMatrix[roleName]["DELIVERY"].Contains('S') ? tab_control : null;
            tab_positions.Parent = AuthorityMatrix.authorityMatrix[roleName]["POSITIONS"].Equals("SDIU") ? tab_control : null;
            tab_prodlist.Parent = AuthorityMatrix.authorityMatrix[roleName]["PRODLIST"].Equals("SDIU") ? tab_control : null;
            tab_prodtype.Parent = AuthorityMatrix.authorityMatrix[roleName]["PRODTYPE"].Equals("SDIU") ? tab_control : null;
            tab_suppliers.Parent = AuthorityMatrix.authorityMatrix[roleName]["SUPPLIERS"].Equals("SDIU") ? tab_control : null;
            tab_orders.Parent = AuthorityMatrix.authorityMatrix[roleName]["ORDERS"].Contains('S') ? tab_control : null;
            tab_employees.Parent = AuthorityMatrix.authorityMatrix[roleName]["EMPLOYEES"].Equals("SDIU") ? tab_control : null;
            tab_shops.Parent = AuthorityMatrix.authorityMatrix[roleName]["SHOPS"].Equals("SDIU") ? tab_control : null;
            tab_offices.Parent = AuthorityMatrix.authorityMatrix[roleName]["OFFICES"].Equals("SDIU") ? tab_control : null;
            tab_serviceord.Parent = AuthorityMatrix.authorityMatrix[roleName]["SERVICEORD"].Contains('S') ? tab_control : null;
            tab_tnsforder.Parent = AuthorityMatrix.authorityMatrix[roleName]["TNSFORDER"].Contains('S') ? tab_control : null;
            tab_servicectr.Parent = AuthorityMatrix.authorityMatrix[roleName]["SERVICECTR"].Equals("SDIU") ? tab_control : null;
            tab_servicelog.Parent = AuthorityMatrix.authorityMatrix[roleName]["LOGSERVICE"].Equals("SDIU") ? tab_control : null;
            tab_prodinshop.Parent = AuthorityMatrix.authorityMatrix[roleName]["PRODINSHOP"].Contains('S') ? tab_control : null;

            btn_prodinstock_add.Visible = AuthorityMatrix.authorityMatrix[roleName]["PRODINSTOCK"].Contains('I');
            btn_prodinstock_del.Visible = AuthorityMatrix.authorityMatrix[roleName]["PRODINSTOCK"].Contains('D');
            btn_orders_add.Visible = AuthorityMatrix.authorityMatrix[roleName]["ORDERS"].Contains('I');
            btn_orders_del.Visible = AuthorityMatrix.authorityMatrix[roleName]["ORDERS"].Contains('D');
            btn_orders_upd.Visible = AuthorityMatrix.authorityMatrix[roleName]["ORDERS"].Contains('U');
            btn_addlog.Visible = AuthorityMatrix.authorityMatrix[roleName]["ORDERS"].Contains('U');
            btn_servicectr_add.Visible = AuthorityMatrix.authorityMatrix[roleName]["ORDERS"].Contains('U') && AuthorityMatrix.authorityMatrix[roleName]["SERVICECTR"].Contains('I');
            btn_prodinshop_add.Visible = AuthorityMatrix.authorityMatrix[roleName]["PRODINSHOP"].Contains('I');
            btn_prodinshop_del.Visible = AuthorityMatrix.authorityMatrix[roleName]["PRODINSHOP"].Contains('D');
            btn_prodinshop_upd.Visible = AuthorityMatrix.authorityMatrix[roleName]["PRODINSHOP"].Contains('U');
            btn_inprogress.Visible = AuthorityMatrix.authorityMatrix[roleName]["SERVICEORD"].Contains('U');
            btn_stopped.Visible = AuthorityMatrix.authorityMatrix[roleName]["SERVICEORD"].Contains('U');
            btn_done.Visible = AuthorityMatrix.authorityMatrix[roleName]["SERVICEORD"].Contains('U');
            btn_decline.Visible = AuthorityMatrix.authorityMatrix[roleName]["SERVICEORD"].Contains('U');
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (mySqlConnect != null) mySqlConnect.Close();
        }

        private void btn_query_Click(object sender, EventArgs e)
        {
            try
            {
                query = rtb_query.Text;
                mySqlCommand = new MySqlCommand(query, mySqlConnect);
                mySqlCommand.ExecuteNonQuery();
                rtb_logs.AppendText("Successs\n");
            }
            catch (Exception _ex)
            {
                rtb_logs.AppendText("Error during query: " + _ex.Message.ToString() + "\n\n");
            }
        }

        private void tab_prodinstock_Click(object sender, EventArgs e)
        {
            refresh_prodinstock();
        }

        private void refresh_prodinstock()
        {
            try
            {
                query = @"SELECT PROD.ProductName AS Наименование, PROD.ProductType AS Тип, COUNT(PROD.ProductName) AS Количество 
                          FROM (SELECT PRODLIST.Name AS ProductName, PRODTYPE.Name AS ProductType FROM PRODINSTOCK, PRODLIST, PRODTYPE 
                          WHERE PRODINSTOCK.Product=PRODLIST.ID AND PRODLIST.Type=PRODTYPE.ID) AS PROD GROUP BY PROD.ProductName;";
                renew_data_table("PRODINSTOCK", query);
                dgv_prodinstock.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void replicate_prodinstock()
        {
            try
            {
                query = @"SELECT PROD.ProductName AS Наименование, PROD.ProductType AS Тип, COUNT(PROD.ProductName) AS Количество 
                          FROM (SELECT PRODLIST.Name AS ProductName, PRODTYPE.Name AS ProductType FROM PRODINSTOCK, PRODLIST, PRODTYPE 
                          WHERE PRODINSTOCK.Product=PRODLIST.ID AND PRODLIST.Type=PRODTYPE.ID) AS PROD GROUP BY PROD.ProductName;";
                renew_data_table_from_host("PRODINSTOCK", query);
                dgv_prodinstock.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_prodinshop()
        {
            try
            {
                query = @"SELECT PRODINSHOP.ID as Номер_товара, PRODLIST.Name as Наименование, PRODLIST.Manufact as Производитель, PRODTYPE.Name as Тип, PRODINSHOP.Price as Цена, PRODINSHOP.Warranty as Гарантия
                          FROM PRODINSHOP, PRODLIST, PRODTYPE
                          WHERE PRODINSHOP.Product=PRODLIST.ID AND PRODLIST.Type=PRODTYPE.ID;";
                renew_data_table("PRODINSHOP", query);
                dgv_prodinshop.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_prodlist()
        {
            try
            {
                query = @"SELECT * FROM PRODLIST;";
                renew_data_table("PRODLIST", query);
                dgv_prodlist.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_prodtype()
        {
            try
            {
                query = @"SELECT * FROM PRODTYPE;";
                renew_data_table("PRODTYPE", query);
                dgv_prodtype.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_employees()
        {
            try
            {
                query = @"SELECT EMPLOYEES.ID AS Номер, EMPLOYEES.Surname as Фамилия, EMPLOYEES.Name as Имя, EMPLOYEES.Middlename as Отчество, EMPLOYEES.BirthDate as Дата_рождения, POS.Name as Должность, POS.Salary as Оклад, SHOP.Address as Магазин, LOG.Name as Служба_логистики, OFC.Name as Офис, SVC.Name as Сервис_центр
                          FROM EMPLOYEES 
                          LEFT JOIN SHOPS AS SHOP ON EMPLOYEES.Shops=SHOP.Id
                          LEFT JOIN LOGSERVICE AS LOG ON EMPLOYEES.Logservice=LOG.Id
                          LEFT JOIN OFFICES AS OFC ON EMPLOYEES.Office=OFC.Id
                          LEFT JOIN POSITIONS AS POS ON EMPLOYEES.Position=POS.Id
                          LEFT JOIN SERVICECTR AS SVC ON EMPLOYEES.Servicectr=SVC.Id;";
                renew_data_table("EMPLOYEES", query);
                dgv_employees.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_positions()
        {
            try
            {
                query = @"SELECT * FROM POSITIONS;";
                renew_data_table("POSITIONS", query);
                dgv_positions.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_orders()
        {
            try
            {
                query = @"SELECT ORD.pin as Номер_заказа, ORD.Name as Наименование_товара, ORD.Price as Цена, LOG.Name as Служба_доставки, ORD.Paid as Оплачено
                          FROM (
	                            SELECT ORDERS.ID as pin, ORDERS.Logservice, ORDERS.Price, ORDERS.Paid, PRODLIST.Name 
	                            FROM ORDERS, PRODLIST 
	                            WHERE ORDERS.Product=PRODLIST.Id
                               ) AS ORD
                          LEFT JOIN LOGSERVICE AS LOG ON ORD.Logservice=LOG.Id;";
                renew_data_table("ORDERS", query);
                dgv_orders.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_tnsforders()
        {
            try
            {
                query = @"SELECT TNSFORDER.ID AS Номер_заявки, PRODLIST.NAME AS Товар, SHOPS.ADDRESS AS Пункт_назначения, LOGSERVICE.NAME AS Служба_доставки
                          FROM TNSFORDER, PRODINSTOCK, PRODLIST, SHOPS, LOGSERVICE
                          WHERE TNSFORDER.Product=PRODINSTOCK.ID AND PRODINSTOCK.Product=PRODLIST.ID AND TNSFORDER.SHOP=SHOPS.ID AND TNSFORDER.LOGSERVICE=LOGSERVICE.ID;";
                renew_data_table("TNSFORDER", query);
                dgv_tnsforder.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_serviceord()
        {
            try
            {
                query = @"SELECT SERVICEORD.ID AS Номер_заявки, ORDERS.ID AS Номер_заказа, SERVICECTR.NAME AS Сервис_центр, PRODLIST.NAME AS Товар, SERVICEORD.STATUS AS Статус_заявки
                          FROM SERVICEORD, SERVICECTR, ORDERS, PRODLIST
                          WHERE SERVICEORD.SERVICECTR=SERVICECTR.ID AND SERVICEORD.ORD=ORDERS.ID AND ORDERS.PRODUCT=PRODLIST.ID;";
                renew_data_table("SERVICEORD", query);
                dgv_serviceord.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_delivery()
        {
            try
            {
                query = @"SELECT DELIVERY.ID AS Номер_заявки, SUPPLIERS.NAME AS Поставщик, PRODLIST.NAME AS Товар, DELIVERY.DATE AS Дата_заявки, DELIVERY.COUNT AS Количество
                          FROM DELIVERY, SUPPLIERS, PRODLIST
                          WHERE DELIVERY.SUPPLIER=SUPPLIERS.ID AND DELIVERY.PRODUCT=PRODLIST.ID;";
                renew_data_table("DELIVERY", query);
                dgv_delivery.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_suppliers()
        {
            try
            {
                query = @"SELECT * FROM SUPPLIERS;";
                renew_data_table("SUPPLIERS", query);
                dgv_suppliers.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_shops()
        {
            try
            {
                query = @"SELECT * FROM SHOPS;";
                renew_data_table("SHOPS", query);
                dgv_shops.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_offices()
        {
            try
            {
                query = @"SELECT * FROM OFFICES;";
                renew_data_table("OFFICES", query);
                dgv_offices.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_servicectr()
        {
            try
            {
                query = @"SELECT * FROM SERVICECTR;";
                renew_data_table("SERVICECTR", query);
                dgv_servicectr.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void refresh_logservice()
        {
            try
            {
                query = @"SELECT * FROM LOGSERVICE;";
                renew_data_table("LOGSERVICES", query);
                dgv_logservice.Refresh();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_prodinstock_add_Click(object sender, EventArgs e)
        {
            InsertProdInStock insertProd = new InsertProdInStock(mySqlConnect, 1);
            insertProd.ShowDialog();
            if (insertProd.DialogResult == DialogResult.Yes)
            {
                try
                {
                    int insertedProduct = insertProd.getSelectedProd();
                    query = @"INSERT INTO PRODINSTOCK(PRODUCT)
                          VALUES(@insertedProduct)";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@insertedProduct", insertedProduct);
                    mySqlCommand.ExecuteNonQuery();
                    refresh_prodinstock();
                }
                catch (Exception _ex)
                {
                    MessageBox.Show("Ошибка добавления данных: " + _ex.Message.ToString());
                }
            }
        }

        private void btn_prodinstock_del_Click(object sender, EventArgs e)
        {
            InsertProdInStock insertProd = new InsertProdInStock(mySqlConnect, 2);
            insertProd.ShowDialog();
            refresh_prodinstock();
        }

        private void btn_prodinshop_add_Click(object sender, EventArgs e)
        {
            try
            {
                int productID;
                int shopID;
                double price;
                double warranty;
                Prodinshop prodInShop = new Prodinshop(mySqlConnect);
                prodInShop.ShowDialog();
                if (prodInShop.DialogResult == DialogResult.Yes)
                {
                    prodInShop.getParameters(out productID, out price, out warranty, out shopID);
                    query = @"INSERT INTO PRODINSHOP(PRODUCT,SHOP,PRICE,WARRANTY)
                          VALUES(@productID, @shopID, @price, @Warranty)";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@productID", productID);
                    mySqlCommand.Parameters.AddWithValue("@shopID", shopID);
                    mySqlCommand.Parameters.AddWithValue("@price", price);
                    mySqlCommand.Parameters.AddWithValue("@warranty", warranty);
                    mySqlCommand.ExecuteNonQuery();
                    refresh_prodinshop();
                }
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка добавления данных: " + _ex.Message.ToString());
            }
        }

        private void dgv_prodinshop_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_prodinshop.Rows.Count)
                    return;
                if (dgv_prodinshop.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                tb_price.Text = dgv_prodinshop.Rows[e.RowIndex].Cells[4].Value.ToString();
                tb_war.Text = dgv_prodinshop.Rows[e.RowIndex].Cells[5].Value.ToString();
                prodinshopID = Convert.ToInt32(dgv_prodinshop.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        private void btn_prodinshop_upd_Click(object sender, EventArgs e)
        {
            try
            {
                query = @"UPDATE PRODINSHOP SET PRICE=@Price, WARRANTY=@Warranty WHERE ID=@prodinshopId";
                mySqlCommand = new MySqlCommand(query, mySqlConnect);
                mySqlCommand.Parameters.AddWithValue("@prodinshopId", prodinshopID);
                mySqlCommand.Parameters.AddWithValue("@Price", Convert.ToDouble(tb_price.Text));
                mySqlCommand.Parameters.AddWithValue("@Warranty", Convert.ToDouble(tb_war.Text));
                mySqlCommand.ExecuteNonQuery();
                refresh_prodinshop();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка обновления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_prodinshop_del_Click(object sender, EventArgs e)
        {
            try
            {
                query = @"DELETE FROM Prodinshop WHERE ID=@prodinshopID";
                mySqlCommand = new MySqlCommand(query, mySqlConnect);
                mySqlCommand.Parameters.AddWithValue("@prodinshopId", prodinshopID);
                mySqlCommand.ExecuteNonQuery();
                refresh_prodinshop();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка удаления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_prodlist_add_Click(object sender, EventArgs e)
        {
            try
            {
                int typeID;
                string name;
                string manufacturer;
                InsertProdList prodList = new InsertProdList(mySqlConnect);
                prodList.ShowDialog();
                if (prodList.DialogResult == DialogResult.Yes)
                {
                    prodList.getParameters(out typeID, out name, out manufacturer);
                    query = @"INSERT INTO PRODLIST(NAME,MANUFACT,TYPE,PARAMS)
                          VALUES(@name, @manufact, @typeID,@params)";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@name", name);
                    mySqlCommand.Parameters.AddWithValue("@manufact", manufacturer);
                    mySqlCommand.Parameters.AddWithValue("@typeID", typeID);
                    mySqlCommand.Parameters.AddWithValue("@params", "");
                    mySqlCommand.ExecuteNonQuery();
                    refresh_prodlist();
                }
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка добавления данных: " + _ex.Message.ToString());
            }
        }

        private void dgv_prodlist_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_prodlist.Rows.Count)
                    return;
                if (dgv_prodlist.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                tb_prodlist_name.Text = dgv_prodlist.Rows[e.RowIndex].Cells[1].Value.ToString();
                tb_prodlist_man.Text = dgv_prodlist.Rows[e.RowIndex].Cells[2].Value.ToString();
                prodlistID = Convert.ToInt32(dgv_prodlist.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        private void btn_prodlist_del_Click(object sender, EventArgs e)
        {
            try
            {
                query = @"DELETE FROM Prodlist WHERE ID=@prodlistID";
                mySqlCommand = new MySqlCommand(query, mySqlConnect);
                mySqlCommand.Parameters.AddWithValue("@prodlistId", prodlistID);
                mySqlCommand.ExecuteNonQuery();
                refresh_prodlist();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка удаления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_prodlist_upd_Click(object sender, EventArgs e)
        {
            try
            {
                query = @"UPDATE PRODLIST SET NAME=@Name, MANUFACT=@Manufact WHERE ID=@prodlistId";
                mySqlCommand = new MySqlCommand(query, mySqlConnect);
                mySqlCommand.Parameters.AddWithValue("@prodlistId", prodlistID);
                mySqlCommand.Parameters.AddWithValue("@Name", tb_prodlist_name.Text.ToString());
                mySqlCommand.Parameters.AddWithValue("@Manufact", tb_prodlist_man.Text.ToString());
                mySqlCommand.ExecuteNonQuery();
                refresh_prodlist();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка обновления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_prodtype_add_Click(object sender, EventArgs e)
        {
            try
            {
                InsertProdType insertType = new InsertProdType();
                insertType.ShowDialog();
                if (insertType.DialogResult == DialogResult.OK)
                {
                    string typename = insertType.get_name();
                    query = @"INSERT INTO PRODTYPE(NAME)
                              VALUES(@name)";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@name", typename);
                    mySqlCommand.ExecuteNonQuery();
                    refresh_prodtype();
                }
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка добавления данных: " + _ex.Message.ToString());
            }
        }

        private void dgv_prodtype_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_prodtype.Rows.Count)
                    return;
                if (dgv_prodtype.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                tb_typename.Text = dgv_prodtype.Rows[e.RowIndex].Cells[1].Value.ToString();
                prodtypeID = Convert.ToInt32(dgv_prodtype.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        private void btn_prodtype_upd_Click(object sender, EventArgs e)
        {
            try
            {
                query = @"UPDATE PRODTYPE SET NAME=@Name WHERE ID=@prodtypeId";
                mySqlCommand = new MySqlCommand(query, mySqlConnect);
                mySqlCommand.Parameters.AddWithValue("@prodtypeId", prodtypeID);
                mySqlCommand.Parameters.AddWithValue("@Name", tb_typename.Text.ToString());
                mySqlCommand.ExecuteNonQuery();
                refresh_prodtype();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка обновления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_prodtype_del_Click(object sender, EventArgs e)
        {
            try
            {
                query = @"DELETE FROM Prodtype WHERE ID=@prodtypeID";
                mySqlCommand = new MySqlCommand(query, mySqlConnect);
                mySqlCommand.Parameters.AddWithValue("@prodtypeId", prodtypeID);
                mySqlCommand.ExecuteNonQuery();
                refresh_prodtype();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка удаления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_orders_add_Click(object sender, EventArgs e)
        {
            try
            {
                PurchaseOrder orderForm = new PurchaseOrder(mySqlConnect);
                orderForm.ShowDialog();
                if (orderForm.DialogResult == DialogResult.OK)
                {
                    int _prodID;
                    double _price;
                    orderForm.get_parameters(out _prodID, out _price);
                    query = @"INSERT INTO ORDERS(PRODUCT, LOGSERVICE, DATE, PRICE, PAID)
                              VALUES(@prodID, @LogService, curdate(), @Price, @Paid)";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@prodID", _prodID);
                    mySqlCommand.Parameters.AddWithValue("@Price", _price);
                    mySqlCommand.Parameters.AddWithValue("@Paid", false);
                    mySqlCommand.Parameters.AddWithValue("@LogService", null);
                    mySqlCommand.ExecuteNonQuery();
                    refresh_orders();

                    /*
                    query = @"DELETE FROM Prodinshop WHERE ID=@prodID";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@prodID", _prodID);
                    mySqlCommand.ExecuteNonQuery();
                    */
                }
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка добавления данных: " + _ex.Message.ToString());
            }

        }

        private void dgv_orders_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_orders.Rows.Count)
                    return;
                if (dgv_orders.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                orderID = Convert.ToInt32(dgv_orders.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        private void btn_addlog_Click(object sender, EventArgs e)
        {
            try
            {
                AddLogService addLogService = new AddLogService(mySqlConnect);
                addLogService.ShowDialog();
                if (addLogService.DialogResult == DialogResult.OK)
                {
                    int _logservicID = addLogService.get_logserviceID();
                    query = @"UPDATE ORDERS SET LOGSERVICE=@logID WHERE ID=@orderID";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@logID", _logservicID);
                    mySqlCommand.Parameters.AddWithValue("@orderID", orderID);
                    mySqlCommand.ExecuteNonQuery();
                    refresh_orders();
                }
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка обновления данных: " + _ex.Message.ToString());
            } 
        }

        private void btn_orders_upd_Click(object sender, EventArgs e)
        {
            try
            {
                query = @"UPDATE ORDERS SET PAID=@Paid WHERE ID=@orderID";
                mySqlCommand = new MySqlCommand(query, mySqlConnect);
                mySqlCommand.Parameters.AddWithValue("@Paid", true);
                mySqlCommand.Parameters.AddWithValue("@orderID", orderID);
                mySqlCommand.ExecuteNonQuery();
                refresh_orders();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка обновления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_orders_del_Click(object sender, EventArgs e)
        {
            try
            {
                query = @"DELETE FROM Orders WHERE ID=@orderID";
                mySqlCommand = new MySqlCommand(query, mySqlConnect);
                mySqlCommand.Parameters.AddWithValue("@orderId", orderID);
                mySqlCommand.ExecuteNonQuery();
                refresh_orders();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка удаления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_servicectr_add_Click(object sender, EventArgs e)
        {
            try
            {
                AddServiceCtr addServiceCtr = new AddServiceCtr(mySqlConnect);
                addServiceCtr.ShowDialog();
                if (addServiceCtr.DialogResult == DialogResult.OK)
                {
                    int _servicectrID = addServiceCtr.get_servicectrID();
                    query = @"INSERT INTO SERVICEORD(SERVICECTR,ORD,STATUS)
                              VALUES(@servicectrID, @orderID, @Status)";
                    mySqlCommand = new MySqlCommand(query, mySqlConnect);
                    mySqlCommand.Parameters.AddWithValue("@servicectrID", _servicectrID);
                    mySqlCommand.Parameters.AddWithValue("@orderID", orderID);
                    mySqlCommand.Parameters.AddWithValue("@Status", "NEW");
                    mySqlCommand.ExecuteNonQuery();
                    refresh_serviceord();
                }
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка добавления данных: " + _ex.Message.ToString());
            }

        }

        private void update_status(string _status)
        {
            try
            {
                query = @"UPDATE SERVICEORD SET STATUS=@Status WHERE ID=@serviceordID";
                mySqlCommand = new MySqlCommand(query, mySqlConnect);
                mySqlCommand.Parameters.AddWithValue("@Status", _status);
                mySqlCommand.Parameters.AddWithValue("@serviceordID", serviceordID);
                mySqlCommand.ExecuteNonQuery();
                refresh_serviceord();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка обновления данных: " + _ex.Message.ToString());
            }
        }

        private void dgv_serviceord_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == dgv_serviceord.Rows.Count)
                    return;
                if (dgv_serviceord.Rows[e.RowIndex].Cells[0].Value == DBNull.Value)
                    return;
                serviceordID = Convert.ToInt32(dgv_serviceord.Rows[e.RowIndex].Cells[0].Value);
            }
            catch (Exception _ex)
            {
                MessageBox.Show(_ex.Message.ToString());
            }
        }

        private void btn_inprogress_Click(object sender, EventArgs e)
        {
            update_status("IN PROGRESS");
        }

        private void btn_stopped_Click(object sender, EventArgs e)
        {
            update_status("STOPPED");
        }

        private void btn_done_Click(object sender, EventArgs e)
        {
            update_status("DONE");
        }

        private void btn_decline_Click(object sender, EventArgs e)
        {
            update_status("DECLINE");
        }

        private void link_profit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                query = @"SELECT ORD.NAME AS Товар, SUM(ORD.PRICE) AS Выручка
                      FROM (
                            SELECT ORDERS.PRICE, PRODTYPE.ID, PRODTYPE.NAME
                            FROM ORDERS, PRODINSHOP, PRODLIST, PRODTYPE
                            WHERE ORDERS.PRODUCT=PRODINSHOP.ID AND PRODINSHOP.PRODUCT=PRODLIST.ID AND PRODLIST.TYPE=PRODTYPE.ID AND ORDERS.PAID=True
                           ) AS ORD
                      GROUP BY ORD.ID;";
                ShowReport showReport = new ShowReport(mySqlConnect, query);
                showReport.ShowDialog();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void link_employees_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                query = @"SELECT EMPLOYEES.Surname AS Фамилия, EMPLOYEES.Name AS Имя, EMPLOYEES.MiddleName AS Отчество, EMPLOYEES.BirthDate AS Дата_рождения, Positions.name as Должность
                          FROM EMPLOYEES 
                          INNER JOIN positions ON EMPLOYEES.position=positions.ID;";
                ShowReport showReport = new ShowReport(mySqlConnect, query);
                showReport.ShowDialog();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Ошибка предоставления данных: " + _ex.Message.ToString());
            }
        }

        private void btn_employees_add_Click(object sender, EventArgs e)
        {
            AddEmployee addEmployee = new AddEmployee(mySqlConnect);
            addEmployee.ShowDialog();
            if (addEmployee.DialogResult == DialogResult.Yes)
            {
                refresh_employees();
            }
        }

        private void btn_rep_Click(object sender, EventArgs e)
        {
            replicate_prodinstock();
        }
    }
}
