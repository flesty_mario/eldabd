﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EldaVideo
{
    public static class AuthorityMatrix
    {
        public static Dictionary<string, Dictionary<string, string>> authorityMatrix = new Dictionary<string, Dictionary<string, string>>();
        private static Dictionary<string, string> authorityMatrixMerchant = new Dictionary<string, string>();
        private static Dictionary<string, string> authorityMatrixAdmin = new Dictionary<string, string>();
        private static Dictionary<string, string> authorityMatrixSeller = new Dictionary<string, string>();
        private static Dictionary<string, string> authorityMatrixAccountant = new Dictionary<string, string>();
        private static Dictionary<string, string> authorityMatrixServiceWorker = new Dictionary<string, string>();
        private static Dictionary<string, string> authorityMatrixLogisticWorker = new Dictionary<string, string>();
        private static Dictionary<string, string> authorityMatrixHR = new Dictionary<string, string>();
        
        public static void setMatrix()
        {
            authorityMatrixMerchant.Add("DELIVERY", "SDIU");
            authorityMatrixMerchant.Add("EMPLOYEES", "");
            authorityMatrixMerchant.Add("LOGSERVICE", "");
            authorityMatrixMerchant.Add("OFFICES", "");
            authorityMatrixMerchant.Add("ORDERS", "");
            authorityMatrixMerchant.Add("POSITIONS", "");
            authorityMatrixMerchant.Add("PRODINSHOP", "SDIU");
            authorityMatrixMerchant.Add("PRODINSTOCK", "SDIU");
            authorityMatrixMerchant.Add("PRODLIST", "SDIU");
            authorityMatrixMerchant.Add("PRODTYPE", "SDIU");
            authorityMatrixMerchant.Add("SERVICECTR", "");
            authorityMatrixMerchant.Add("SERVICEORD", "");
            authorityMatrixMerchant.Add("SHOPS", "");
            authorityMatrixMerchant.Add("SUPPLIERS", "");
            authorityMatrixMerchant.Add("TNSFORDER", "");
            authorityMatrixMerchant.Add("CONSOLE", "");

            authorityMatrixAdmin.Add("DELIVERY", "SDIU");
            authorityMatrixAdmin.Add("EMPLOYEES", "SDIU");
            authorityMatrixAdmin.Add("LOGSERVICE", "SDIU");
            authorityMatrixAdmin.Add("OFFICES", "SDIU");
            authorityMatrixAdmin.Add("ORDERS", "SDIU");
            authorityMatrixAdmin.Add("POSITIONS", "SDIU");
            authorityMatrixAdmin.Add("PRODINSHOP", "SDIU");
            authorityMatrixAdmin.Add("PRODINSTOCK", "SDIU");
            authorityMatrixAdmin.Add("PRODLIST", "SDIU");
            authorityMatrixAdmin.Add("PRODTYPE", "SDIU");
            authorityMatrixAdmin.Add("SERVICECTR", "SDIU");
            authorityMatrixAdmin.Add("SERVICEORD", "SDIU");
            authorityMatrixAdmin.Add("SHOPS", "SDIU");
            authorityMatrixAdmin.Add("SUPPLIERS", "SDIU");
            authorityMatrixAdmin.Add("TNSFORDER", "SDIU");
            authorityMatrixAdmin.Add("CONSOLE", "SDIU");

            authorityMatrixSeller.Add("DELIVERY", "");
            authorityMatrixSeller.Add("EMPLOYEES", "");
            authorityMatrixSeller.Add("LOGSERVICE", "S");
            authorityMatrixSeller.Add("OFFICES", "");
            authorityMatrixSeller.Add("ORDERS", "SDIU");
            authorityMatrixSeller.Add("POSITIONS", "");
            authorityMatrixSeller.Add("PRODINSHOP", "S");
            authorityMatrixSeller.Add("PRODINSTOCK", "S");
            authorityMatrixSeller.Add("PRODLIST", "S");
            authorityMatrixSeller.Add("PRODTYPE", "S");
            authorityMatrixSeller.Add("SERVICECTR", "S");
            authorityMatrixSeller.Add("SERVICEORD", "SI");
            authorityMatrixSeller.Add("SHOPS", "S");
            authorityMatrixSeller.Add("SUPPLIERS", "");
            authorityMatrixSeller.Add("TNSFORDER", "SI");
            authorityMatrixSeller.Add("CONSOLE", "");

            authorityMatrixAccountant.Add("DELIVERY", "");
            authorityMatrixAccountant.Add("EMPLOYEES", "");
            authorityMatrixAccountant.Add("LOGSERVICE", "S");
            authorityMatrixAccountant.Add("OFFICES", "S");
            authorityMatrixAccountant.Add("ORDERS", "S");
            authorityMatrixAccountant.Add("POSITIONS", "");
            authorityMatrixAccountant.Add("PRODINSHOP", "S");
            authorityMatrixAccountant.Add("PRODINSTOCK", "");
            authorityMatrixAccountant.Add("PRODLIST", "S");
            authorityMatrixAccountant.Add("PRODTYPE", "");
            authorityMatrixAccountant.Add("SERVICECTR", "S");
            authorityMatrixAccountant.Add("SERVICEORD", "S");
            authorityMatrixAccountant.Add("SHOPS", "S");
            authorityMatrixAccountant.Add("SUPPLIERS", "");
            authorityMatrixAccountant.Add("TNSFORDER", "S");
            authorityMatrixAccountant.Add("CONSOLE", "");

            authorityMatrixServiceWorker.Add("DELIVERY", "");
            authorityMatrixServiceWorker.Add("EMPLOYEES", "");
            authorityMatrixServiceWorker.Add("LOGSERVICE", "");
            authorityMatrixServiceWorker.Add("OFFICES", "");
            authorityMatrixServiceWorker.Add("ORDERS", "S");
            authorityMatrixServiceWorker.Add("POSITIONS", "");
            authorityMatrixServiceWorker.Add("PRODINSHOP", "S");
            authorityMatrixServiceWorker.Add("PRODINSTOCK", "");
            authorityMatrixServiceWorker.Add("PRODLIST", "S");
            authorityMatrixServiceWorker.Add("PRODTYPE", "S");
            authorityMatrixServiceWorker.Add("SERVICECTR", "S");
            authorityMatrixServiceWorker.Add("SERVICEORD", "SDIU");
            authorityMatrixServiceWorker.Add("SHOPS", "");
            authorityMatrixServiceWorker.Add("SUPPLIERS", "");
            authorityMatrixServiceWorker.Add("TNSFORDER", "");
            authorityMatrixServiceWorker.Add("CONSOLE", "");

            authorityMatrixLogisticWorker.Add("DELIVERY", "");
            authorityMatrixLogisticWorker.Add("EMPLOYEES", "");
            authorityMatrixLogisticWorker.Add("LOGSERVICE", "S");
            authorityMatrixLogisticWorker.Add("OFFICES", "");
            authorityMatrixLogisticWorker.Add("ORDERS", "SU");
            authorityMatrixLogisticWorker.Add("POSITIONS", "");
            authorityMatrixLogisticWorker.Add("PRODINSHOP", "S");
            authorityMatrixLogisticWorker.Add("PRODINSTOCK", "");
            authorityMatrixLogisticWorker.Add("PRODLIST", "S");
            authorityMatrixLogisticWorker.Add("PRODTYPE", "S");
            authorityMatrixLogisticWorker.Add("SERVICECTR", "");
            authorityMatrixLogisticWorker.Add("SERVICEORD", "");
            authorityMatrixLogisticWorker.Add("SHOPS", "");
            authorityMatrixLogisticWorker.Add("SUPPLIERS", "");
            authorityMatrixLogisticWorker.Add("TNSFORDER", "SDIU");
            authorityMatrixLogisticWorker.Add("CONSOLE", "");

            authorityMatrixHR.Add("DELIVERY", "");
            authorityMatrixHR.Add("EMPLOYEES", "SDIU");
            authorityMatrixHR.Add("LOGSERVICE", "SDIU");
            authorityMatrixHR.Add("OFFICES", "SDIU");
            authorityMatrixHR.Add("ORDERS", "");
            authorityMatrixHR.Add("POSITIONS", "SDIU");
            authorityMatrixHR.Add("PRODINSHOP", "");
            authorityMatrixHR.Add("PRODINSTOCK", "");
            authorityMatrixHR.Add("PRODLIST", "");
            authorityMatrixHR.Add("PRODTYPE", "");
            authorityMatrixHR.Add("SERVICECTR", "SDIU");
            authorityMatrixHR.Add("SERVICEORD", "");
            authorityMatrixHR.Add("SHOPS", "SDIU");
            authorityMatrixHR.Add("SUPPLIERS", "");
            authorityMatrixHR.Add("TNSFORDER", "");
            authorityMatrixHR.Add("CONSOLE", "");

            authorityMatrix.Add("Merchant", authorityMatrixMerchant);
            authorityMatrix.Add("Admin", authorityMatrixAdmin);
            authorityMatrix.Add("ServiceWorker", authorityMatrixServiceWorker);
            authorityMatrix.Add("LogWorker", authorityMatrixLogisticWorker);
            authorityMatrix.Add("HR", authorityMatrixHR);
            authorityMatrix.Add("Accountant", authorityMatrixAccountant);
            authorityMatrix.Add("Seller", authorityMatrixSeller);
        }
        
    }
}
