﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace EldaVideo
{
    public partial class Login : Form
    {
        private string sconnect;
        private string user;
        private string host;
        private MySqlConnection mconnect;
        private MySqlConnection mconnectHost;
        public Login()
        {
            InitializeComponent();
            mconnect = null;
            this.DialogResult = DialogResult.No;
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            try
            {
                user = text_user.Text;
                host = tb_host.Text;
                sconnect = @"host=localhost;port=3306;user=" + text_user.Text + ";password=" + text_pwd.Text + ";database=electronic;";
                mconnect = new MySqlConnection(sconnect);
                mconnect.Open();
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Не удается подключиться к БД: " + _ex.Message.ToString());
            }
            try
            {
                sconnect = @"host=" + host + ";port=3306;user=" + text_user.Text + ";password=" + text_pwd.Text + ";database=electronic;";
                mconnectHost = new MySqlConnection(sconnect);
                mconnectHost.Open();
                this.DialogResult = DialogResult.Yes;
            }
            catch (Exception _ex)
            {
                MessageBox.Show("Не удается подключиться к серверной БД: " + _ex.Message.ToString());
            }
        }

        public MySqlConnection getConnection()
        {
            return mconnect;
        }

        public MySqlConnection getConnectionHost()
        {
            return mconnectHost;
        }

        public string getUserName()
        {
            return user;
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.ControlBox = false;
        }
    }
}
