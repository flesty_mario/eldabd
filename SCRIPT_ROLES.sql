DROP USER 'Admin'@'%';
DROP USER 'Seller'@'%';
DROP USER 'ServiceWorker'@'%';
DROP USER 'LogWorker'@'%';
DROP USER 'Accountant'@'%';
DROP USER 'Merchant'@'%';
DROP USER 'HR'@'%';

CREATE USER 'Admin'@'%' IDENTIFIED BY '123456';
GRANT ALL PRIVILEGES ON electronic.* TO 'Admin'@'%';



CREATE USER 'Seller'@'%' IDENTIFIED BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'Seller'@'%';
GRANT SELECT ON electronic.PRODINSHOP TO 'Seller'@'%';
GRANT SELECT ON electronic.PRODINSTOCK TO 'Seller'@'%';
GRANT SELECT ON electronic.PRODLIST TO 'Seller'@'%';
GRANT SELECT ON electronic.ORDERS TO 'Seller'@'%';
GRANT DELETE ON electronic.ORDERS TO 'Seller'@'%';
GRANT INSERT ON electronic.ORDERS TO 'Seller'@'%';
GRANT UPDATE ON electronic.ORDERS TO 'Seller'@'%';
GRANT SELECT ON electronic.SERVICEORD TO 'Seller'@'%';
GRANT INSERT ON electronic.SERVICEORD TO 'Seller'@'%';
GRANT SELECT ON electronic.TNSFORDER TO 'Seller'@'%';
GRANT INSERT ON electronic.TNSFORDER TO 'Seller'@'%';
GRANT SELECT ON electronic.SHOPS TO 'Seller'@'%';
GRANT SELECT ON electronic.SERVICECTR TO 'Seller'@'%';
GRANT SELECT ON electronic.LOGSERVICE TO 'Seller'@'%';
GRANT SELECT ON electronic.PRODTYPE TO 'Seller'@'%';



CREATE USER 'ServiceWorker'@'%' IDENTIFIED BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'ServiceWorker'@'%';
GRANT SELECT ON electronic.SERVICEORD TO 'ServiceWorker'@'%';
GRANT DELETE ON electronic.SERVICEORD TO 'ServiceWorker'@'%';
GRANT INSERT ON electronic.SERVICEORD TO 'ServiceWorker'@'%';
GRANT UPDATE ON electronic.SERVICEORD TO 'ServiceWorker'@'%';
GRANT SELECT ON electronic.PRODLIST TO 'ServiceWorker'@'%';
GRANT SELECT ON electronic.ORDERS TO 'ServiceWorker'@'%';
GRANT SELECT ON electronic.SERVICECTR TO 'ServiceWorker'@'%';
GRANT SELECT ON electronic.LOGSERVICE TO 'ServiceWorker'@'%';
GRANT SELECT ON electronic.PRODTYPE TO 'ServiceWorker'@'%';
GRANT SELECT ON electronic.PRODINSHOP TO 'ServiceWorker'@'%';



CREATE USER 'LogWorker'@'%' IDENTIFIED BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'LogWorker'@'%';
GRANT SELECT ON electronic.TNSFORDER TO 'LogWorker'@'%';
GRANT DELETE ON electronic.TNSFORDER TO 'LogWorker'@'%';
GRANT INSERT ON electronic.TNSFORDER TO 'LogWorker'@'%';
GRANT UPDATE ON electronic.TNSFORDER TO 'LogWorker'@'%';
GRANT SELECT ON electronic.ORDERS TO 'LogWorker'@'%';
GRANT SELECT ON electronic.LOGSERVICE TO 'LogWorker'@'%';
GRANT SELECT ON electronic.PRODINSHOP TO 'LogWorker'@'%';
GRANT SELECT ON electronic.PRODLIST TO 'LogWorker'@'%';
GRANT SELECT ON electronic.PRODTYPE TO 'LogWorker'@'%';



CREATE USER 'Merchant'@'%' IDENTIFIED BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'Merchant'@'%';
GRANT SELECT ON electronic.PRODINSTOCK TO 'Merchant'@'%';
GRANT DELETE ON electronic.PRODINSTOCK TO 'Merchant'@'%';
GRANT INSERT ON electronic.PRODINSTOCK TO 'Merchant'@'%';
GRANT UPDATE ON electronic.PRODINSTOCK TO 'Merchant'@'%';
GRANT SELECT ON electronic.PRODINSHOP TO 'Merchant'@'%';
GRANT DELETE ON electronic.PRODINSHOP TO 'Merchant'@'%';
GRANT INSERT ON electronic.PRODINSHOP TO 'Merchant'@'%';
GRANT UPDATE ON electronic.PRODINSHOP TO 'Merchant'@'%';
GRANT SELECT ON electronic.PRODLIST TO 'Merchant'@'%';
GRANT DELETE ON electronic.PRODLIST TO 'Merchant'@'%';
GRANT UPDATE ON electronic.PRODLIST TO 'Merchant'@'%';
GRANT INSERT ON electronic.PRODLIST TO 'Merchant'@'%';
GRANT SELECT ON electronic.PRODTYPE TO 'Merchant'@'%';
GRANT DELETE ON electronic.PRODTYPE TO 'Merchant'@'%';
GRANT UPDATE ON electronic.PRODTYPE TO 'Merchant'@'%';
GRANT INSERT ON electronic.PRODTYPE TO 'Merchant'@'%';
GRANT SELECT ON electronic.DELIVERY TO 'Merchant'@'%';
GRANT DELETE ON electronic.DELIVERY TO 'Merchant'@'%';
GRANT INSERT ON electronic.DELIVERY TO 'Merchant'@'%';
GRANT UPDATE ON electronic.DELIVERY TO 'Merchant'@'%';




CREATE USER 'Accountant'@'%' IDENTIFIED BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'Accountant'@'%';
GRANT SELECT ON electronic.OFFICES TO 'Accountant'@'%';
GRANT SELECT ON electronic.SHOPS TO 'Accountant'@'%';
GRANT SELECT ON electronic.ORDERS TO 'Accountant'@'%';
GRANT SELECT ON electronic.SERVICEORD TO 'Accountant'@'%';
GRANT SELECT ON electronic.TNSFORDER TO 'Accountant'@'%';
GRANT SELECT ON electronic.PRODINSHOP TO 'Accountant'@'%';
GRANT SELECT ON electronic.PRODLIST TO 'Accountant'@'%';
GRANT SELECT ON electronic.SERVICECTR TO 'Accountant'@'%';
GRANT SELECT ON electronic.LOGSERVICE TO 'Accountant'@'%';
GRANT SELECT ON electronic.PRODTYPE TO 'Accountant'@'%';
GRANT SELECT ON electronic.PRODINSTOCK TO 'Accountant'@'%';



CREATE USER 'HR'@'%' IDENTIFIED BY '123456';
GRANT REPLICATION SLAVE ON *.* TO 'HR'@'%';
GRANT SELECT ON electronic.EMPLOYEES TO 'HR'@'%';
GRANT DELETE ON electronic.EMPLOYEES TO 'HR'@'%';
GRANT INSERT ON electronic.EMPLOYEES TO 'HR'@'%';
GRANT UPDATE ON electronic.EMPLOYEES TO 'HR'@'%';
GRANT SELECT ON electronic.POSITIONS TO 'HR'@'%';
GRANT DELETE ON electronic.POSITIONS TO 'HR'@'%';
GRANT INSERT ON electronic.POSITIONS TO 'HR'@'%';
GRANT UPDATE ON electronic.POSITIONS TO 'HR'@'%';
GRANT SELECT ON electronic.LOGSERVICE TO 'HR'@'%';
GRANT DELETE ON electronic.LOGSERVICE TO 'HR'@'%';
GRANT INSERT ON electronic.LOGSERVICE TO 'HR'@'%';
GRANT UPDATE ON electronic.LOGSERVICE TO 'HR'@'%';
GRANT SELECT ON electronic.SERVICECTR TO 'HR'@'%';
GRANT DELETE ON electronic.SERVICECTR TO 'HR'@'%';
GRANT INSERT ON electronic.SERVICECTR TO 'HR'@'%';
GRANT UPDATE ON electronic.SERVICECTR TO 'HR'@'%';
GRANT SELECT ON electronic.OFFICES TO 'HR'@'%';
GRANT DELETE ON electronic.OFFICES TO 'HR'@'%';
GRANT INSERT ON electronic.OFFICES TO 'HR'@'%';
GRANT UPDATE ON electronic.OFFICES TO 'HR'@'%';
GRANT SELECT ON electronic.SHOPS TO 'HR'@'%';
GRANT DELETE ON electronic.SHOPS TO 'HR'@'%';
GRANT INSERT ON electronic.SHOPS TO 'HR'@'%';
GRANT UPDATE ON electronic.SHOPS TO 'HR'@'%';