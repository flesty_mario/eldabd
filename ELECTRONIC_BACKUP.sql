-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: electronic
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `delivery`
--

DROP TABLE IF EXISTS `delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `SUPPLIER` int(2) NOT NULL,
  `PRODUCT` int(6) NOT NULL,
  `DATE` date NOT NULL,
  `COUNT` int(4) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `DELIVERY_fk1` (`PRODUCT`),
  KEY `DELIVERY_fk0` (`SUPPLIER`),
  CONSTRAINT `DELIVERY_fk0` FOREIGN KEY (`SUPPLIER`) REFERENCES `suppliers` (`ID`),
  CONSTRAINT `DELIVERY_fk1` FOREIGN KEY (`PRODUCT`) REFERENCES `prodlist` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery`
--

LOCK TABLES `delivery` WRITE;
/*!40000 ALTER TABLE `delivery` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `SURNAME` varchar(100) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  `MIDDLENAME` varchar(100) NOT NULL,
  `BIRTHDATE` date NOT NULL,
  `POSITION` int(2) NOT NULL,
  `LOGSERVICE` int(2) DEFAULT NULL,
  `SHOPS` int(2) DEFAULT NULL,
  `SERVICECTR` int(2) DEFAULT NULL,
  `OFFICE` int(2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `EMPLOYEES_fk0` (`POSITION`),
  KEY `EMPLOYEES_fk1` (`LOGSERVICE`),
  KEY `EMPLOYEES_fk2` (`SHOPS`),
  KEY `EMPLOYEES_fk3` (`SERVICECTR`),
  KEY `EMPLOYEES_fk4` (`OFFICE`),
  CONSTRAINT `EMPLOYEES_fk0` FOREIGN KEY (`POSITION`) REFERENCES `positions` (`ID`),
  CONSTRAINT `EMPLOYEES_fk1` FOREIGN KEY (`LOGSERVICE`) REFERENCES `logservice` (`ID`),
  CONSTRAINT `EMPLOYEES_fk2` FOREIGN KEY (`SHOPS`) REFERENCES `shops` (`ID`),
  CONSTRAINT `EMPLOYEES_fk3` FOREIGN KEY (`SERVICECTR`) REFERENCES `servicectr` (`ID`),
  CONSTRAINT `EMPLOYEES_fk4` FOREIGN KEY (`OFFICE`) REFERENCES `offices` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (2,'Абдульманов','Тимур','Ринатович','1981-04-13',1,1,NULL,NULL,NULL),(3,'Иванов','Иван','Иванович','1988-07-11',1,1,NULL,NULL,NULL),(4,'Петрова','Мария','Ринатовна','1980-04-13',1,2,NULL,NULL,NULL),(5,'Аболдуев','Михаил','Леонидович','1957-01-05',1,NULL,1,NULL,NULL),(6,'Абдульманов','Тимур','Ринатович','1981-04-13',1,NULL,1,NULL,NULL),(7,'Иванов','Иван','Иванович','1988-07-11',1,NULL,1,NULL,NULL),(8,'Петрова','Мария','Ринатовна','1980-04-13',1,NULL,2,NULL,NULL),(9,'Дугина','Олеся','Александровна','1971-04-13',2,NULL,NULL,1,NULL),(10,'Федорова','Федора','Федоровна','1989-07-11',2,NULL,NULL,1,NULL),(11,'Алексеев','Алексей','Алексеевич','1989-07-11',2,NULL,NULL,1,NULL),(12,'Леонова','Ольга','Александровна','1990-04-13',4,NULL,NULL,NULL,1),(13,'Зарипов','Ильдар','Оскарович','1989-07-11',4,NULL,NULL,NULL,1),(14,'Алексеева','Алексия','Алексеевна','1989-07-11',4,NULL,NULL,NULL,1),(15,'Иванова','Елена','Оскаровна','1989-07-11',5,NULL,NULL,NULL,1),(16,'Сидорова','Айгуль','Вартановна','1995-07-11',5,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logservice`
--

DROP TABLE IF EXISTS `logservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logservice` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logservice`
--

LOCK TABLES `logservice` WRITE;
/*!40000 ALTER TABLE `logservice` DISABLE KEYS */;
INSERT INTO `logservice` VALUES (1,'Внутренние линии'),(2,'Центральная');
/*!40000 ALTER TABLE `logservice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `ADDRESS` varchar(250) NOT NULL,
  `NAME` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
INSERT INTO `offices` VALUES (1,'г Москва, ул Тикучева 57, корп, офис 201','Центральный офис \"Эльдавидео\"');
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `PRODUCT` int(6) NOT NULL,
  `LOGSERVICE` int(2) NOT NULL,
  `DATE` date NOT NULL,
  `PRICE` decimal(8,2) NOT NULL,
  `PAID` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ORDERS_fk0` (`PRODUCT`),
  KEY `ORDERS_fk1` (`LOGSERVICE`),
  CONSTRAINT `ORDERS_fk0` FOREIGN KEY (`PRODUCT`) REFERENCES `prodinshop` (`ID`),
  CONSTRAINT `ORDERS_fk1` FOREIGN KEY (`LOGSERVICE`) REFERENCES `logservice` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,1,1,'2017-11-12',30000.00,1),(2,3,1,'2017-10-13',15000.00,1),(3,4,1,'2017-12-13',18000.00,1),(4,10,1,'2017-12-13',18000.00,1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `positions`
--

DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(70) NOT NULL,
  `salary` decimal(8,2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positions`
--

LOCK TABLES `positions` WRITE;
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
INSERT INTO `positions` VALUES (1,'продавец',20000.00),(2,'сотрудник сервис-центра',25000.00),(3,'сотрудник службы логистики и закупки товаров',25000.00),(4,'бухгалтер',30000.00),(5,'сотрудник отдела кадров',30000.00),(6,'Главный бухгалтер',50000.00),(7,'Генеральный директор',90000.00),(8,'Администратор',100000.00);
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodinshop`
--

DROP TABLE IF EXISTS `prodinshop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodinshop` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `PRODUCT` int(6) NOT NULL,
  `SHOP` int(2) NOT NULL,
  `PRICE` decimal(8,2) NOT NULL,
  `WARRANTY` decimal(3,0) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `PRODINSHOP_fk0` (`PRODUCT`),
  KEY `PRODINSHOP_fk1` (`SHOP`),
  CONSTRAINT `PRODINSHOP_fk0` FOREIGN KEY (`PRODUCT`) REFERENCES `prodlist` (`ID`),
  CONSTRAINT `PRODINSHOP_fk1` FOREIGN KEY (`SHOP`) REFERENCES `shops` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodinshop`
--

LOCK TABLES `prodinshop` WRITE;
/*!40000 ALTER TABLE `prodinshop` DISABLE KEYS */;
INSERT INTO `prodinshop` VALUES (1,1,1,20000.00,3),(2,1,1,20000.00,3),(3,2,1,35000.00,4),(4,2,1,35000.00,4),(5,3,1,10000.00,1),(6,6,1,60000.00,16),(7,5,2,20000.00,3),(8,5,2,20000.00,3),(9,5,2,35000.00,4),(10,4,2,35000.00,4),(11,7,2,10000.00,1),(12,2,2,60000.00,16),(13,2,2,60000.00,16);
/*!40000 ALTER TABLE `prodinshop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodinstock`
--

DROP TABLE IF EXISTS `prodinstock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodinstock` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `PRODUCT` int(6) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `PRODINSTOCK_fk0` (`PRODUCT`),
  CONSTRAINT `PRODINSTOCK_fk0` FOREIGN KEY (`PRODUCT`) REFERENCES `prodlist` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodinstock`
--

LOCK TABLES `prodinstock` WRITE;
/*!40000 ALTER TABLE `prodinstock` DISABLE KEYS */;
INSERT INTO `prodinstock` VALUES (1,1),(2,1),(3,1),(4,1),(5,2),(6,2),(7,2),(8,3),(9,3),(10,3),(11,3),(12,4),(13,4),(14,5),(15,5),(16,5),(17,6),(18,6),(19,6),(20,7),(21,7),(22,7);
/*!40000 ALTER TABLE `prodinstock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodlist`
--

DROP TABLE IF EXISTS `prodlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodlist` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `MANUFACT` varchar(50) NOT NULL,
  `TYPE` int(4) NOT NULL,
  `PARAMS` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `PRODLIST_fk0` (`TYPE`),
  CONSTRAINT `PRODLIST_fk0` FOREIGN KEY (`TYPE`) REFERENCES `prodtype` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodlist`
--

LOCK TABLES `prodlist` WRITE;
/*!40000 ALTER TABLE `prodlist` DISABLE KEYS */;
INSERT INTO `prodlist` VALUES (1,'Сервер DL360PG8 SERVER MIDSIZE ENTERPRISE SOLUTION/COLLABORATION','Avaya',1,''),(2,'Медиашлюз G650 MEDIA GATEWAY RHS','Avaya',2,''),(3,'Блок питания G650 AC/DC POWER SUPPLY 655A NON GSA','Avaya',3,''),(4,'Сервер DMA','Polycom',1,''),(5,'Медиашлюз G430 MP120 MEDIA GATEWAY','Avaya',2,''),(6,'ПО CC R6 NEW ELITE PER AGENT 1-100 LIC:CU','Avaya',5,''),(7,'IP-телефон Cisco Unified Phone 8945, Phantom Grey, Slimline Handset','Avaya',8,'');
/*!40000 ALTER TABLE `prodlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodtype`
--

DROP TABLE IF EXISTS `prodtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prodtype` (
  `ID` int(4) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(70) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodtype`
--

LOCK TABLES `prodtype` WRITE;
/*!40000 ALTER TABLE `prodtype` DISABLE KEYS */;
INSERT INTO `prodtype` VALUES (1,'Сервер'),(2,'Медиашлюз'),(3,'Блок'),(4,'Плата'),(5,'ПО'),(6,'Базовая станция'),(7,'Телефон'),(8,'IP-Телефон');
/*!40000 ALTER TABLE `prodtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicectr`
--

DROP TABLE IF EXISTS `servicectr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicectr` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  `ADDRESS` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicectr`
--

LOCK TABLES `servicectr` WRITE;
/*!40000 ALTER TABLE `servicectr` DISABLE KEYS */;
INSERT INTO `servicectr` VALUES (1,'Центральный на Лубянке','г Москва, Театральный пр-д, 5'),(2,'Зездный','г Москва, Звездный бульвар, д. 19'),(3,'Южный','г Москва, ул. Маршала Катукова, д. 25');
/*!40000 ALTER TABLE `servicectr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `serviceord`
--

DROP TABLE IF EXISTS `serviceord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serviceord` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `SERVICECTR` int(2) NOT NULL,
  `ORDER` int(8) NOT NULL,
  `STATUS` varchar(25) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `SERVICEORD_fk0` (`SERVICECTR`),
  KEY `SERVICEORD_fk1` (`ORDER`),
  CONSTRAINT `SERVICEORD_fk0` FOREIGN KEY (`SERVICECTR`) REFERENCES `servicectr` (`ID`),
  CONSTRAINT `SERVICEORD_fk1` FOREIGN KEY (`ORDER`) REFERENCES `orders` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serviceord`
--

LOCK TABLES `serviceord` WRITE;
/*!40000 ALTER TABLE `serviceord` DISABLE KEYS */;
/*!40000 ALTER TABLE `serviceord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops`
--

DROP TABLE IF EXISTS `shops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shops` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `ADDRESS` varchar(250) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops`
--

LOCK TABLES `shops` WRITE;
/*!40000 ALTER TABLE `shops` DISABLE KEYS */;
INSERT INTO `shops` VALUES (1,'г Москва, Ивантеевская улица, 25А,'),(2,'г Москва, Каширское шоссе, д. 96, корпус 1'),(3,'г Москва, Россошанский проезд, дом 3'),(4,'г Москва, Ленинская Слобода, дом 26, стр.2');
/*!40000 ALTER TABLE `shops` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `ID` int(2) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'Атлон'),(2,'Zyxel'),(3,'Nvidia'),(4,'Cisco');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tnsforder`
--

DROP TABLE IF EXISTS `tnsforder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tnsforder` (
  `ID` int(6) NOT NULL AUTO_INCREMENT,
  `PRODUCT` int(6) NOT NULL,
  `SHOP` int(2) NOT NULL,
  `LOGSERVICE` int(2) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `TNSFORDER_fk0` (`PRODUCT`),
  KEY `TNSFORDER_fk1` (`SHOP`),
  KEY `TNSFORDER_fk2` (`LOGSERVICE`),
  CONSTRAINT `TNSFORDER_fk0` FOREIGN KEY (`PRODUCT`) REFERENCES `prodinstock` (`ID`),
  CONSTRAINT `TNSFORDER_fk1` FOREIGN KEY (`SHOP`) REFERENCES `shops` (`ID`),
  CONSTRAINT `TNSFORDER_fk2` FOREIGN KEY (`LOGSERVICE`) REFERENCES `logservice` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tnsforder`
--

LOCK TABLES `tnsforder` WRITE;
/*!40000 ALTER TABLE `tnsforder` DISABLE KEYS */;
/*!40000 ALTER TABLE `tnsforder` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-26 11:15:46
